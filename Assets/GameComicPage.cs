﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace BoletaGaming.Comic
{
    [RequireComponent(typeof(Image))]
    [RequireComponent(typeof(Animator))]
    public class GameComicPage : MonoBehaviour
    {
        [SerializeField] Animator animator;
        [SerializeField] Image image;
        string state;

    }

}
