﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;
using BoletaGaming.WWF.Minigame;
using BoletaGaming.WWF;
using BoletaGaming.HolderCombo;
using BoletaGaming.AudioGraph;
using System.Linq;

namespace BoletaGaming.FindDifferences
{
    public class FindDifferences : Minigame
    {
        [BoxGroup("Database")] public Level[] levels;

        //[BoxGroup("UI")] [SerializeField] private List<Image> imgMainImgs = new List<Image>();
        [BoxGroup("UI")] [SerializeField] private Transform mainHolder;
        private Transform mapTransform;
        public float sFactor = 0.2f;

        [BoxGroup("State")] public Level actLevel;
        [BoxGroup("UI")] [SerializeField] private List<Image> imgDifferences = new List<Image>();
        [BoxGroup("UI")] [SerializeField] private Text counter;
        [NaughtyAttributes.MinMaxSlider(0.5f,8)] private Vector2 zoomRange = new Vector2(1,4);

        public AudioNode audioNode;

        public AudioClipReference winSound;
        public AudioClipReference loseSound;

        public override void MG_End(bool _win)
        {
            base.MG_End(_win);

            if (_win)
            {
                audioNode.PlaySFX(winSound);
            }
            else
            {
                audioNode.PlaySFX(loseSound);
            }
        }

        public override void MG_Pause()
        {
            throw new System.NotImplementedException();
        }

        public override void MG_Quit()
        {
            throw new System.NotImplementedException();
        }

        public override void MG_Reset()
        {
            throw new System.NotImplementedException();
        }

        public override void MG_Start(int _id)
        {
            base.MG_Start(_id);
            actLevel = levels[_id];
            Screen.orientation = ScreenOrientation.Landscape;
            SetUI(actLevel);
        }

        private void Update()
        {
            mapTransform.localScale = (mapTransform.localScale + mapTransform.localScale * Input.GetAxis("Mouse ScrollWheel") * sFactor).Clamp(zoomRange.x, zoomRange.y);
        }

        private void Start()
        {
        }



        public void ClickDifference(Button btn)
        {
            btn.interactable = false;
            imgDifferences.Remove(btn.GetComponent<Image>());
             
            UpdateCounter();

            if (imgDifferences.Count > 0) // keep playing
            {

            }
            else // win
            {
                MG_End(true);
            }
        }

        public void ClickImg(Image img)
        {
            //click fail
        }

        public void PrintState()
        {
            Debug.Log("there are " + imgDifferences.Count);
        }

        public void SetUI(Level value)
        {
            imgDifferences.Clear();
            //imgMainImgs.Clear();
            mainHolder.Clear();

             mapTransform = Instantiate(value.map, mainHolder).transform;

            //imgMainImgs.Add(_map.GetChild(0).GetComponent<Image>());
            //imgMainImgs.Add(_map.GetChild(1).GetComponent<Image>());

            List<Button> butttons = mapTransform.GetComponentsInChildren<Button>().ToList();
            butttons.ForEach(x => { x.onClick.AddListener(() => ClickDifference(x)); });
            imgDifferences.AddRange(butttons.ConvertAll(x => x.GetComponent<Image>()));
            UpdateCounter();

            // imgMainImgs.Add();

        }

        public void UpdateCounter()
        {
            Debug.Log("Faltan " + imgDifferences.Count + " Diferencias....");
            counter.text = "Faltan " + imgDifferences.Count + " Diferencias....";
        }


        public override int GetNumLevels()
        {
            return levels.Length- numMixLevels;
        }

    }


    public class Difference
    {
        public Sprite sprite;
    }

    [System.Serializable]
    public class Level
    {
        public GameObject map;
    }


}
