﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ExecuteWithAnOdd : MonoBehaviour
{
    [Range(0.0f, 1f)][SerializeField]float odd = 0.3f;

    [SerializeField]UnityEvent onEnable;
    [SerializeField]UnityEvent onDisable;
    [SerializeField]UnityEvent onAwake;
    [SerializeField] UnityEvent onStart;


    private bool CheckCondition()
    {
        return Random.Range(0.0f, 1) <= odd;
    }

    private void Awake()
    {
        if (CheckCondition())
        {
            onAwake.Invoke();
        }
    }

    private void Start()
    {
        if (CheckCondition())
        {
            onStart.Invoke();
        }
    }

    private void OnDisable()
    {
        if (CheckCondition())
        {
            onDisable.Invoke();
        }
    }

    private void OnEnable()
    {
        if (CheckCondition())
        {
            onEnable.Invoke();
        }
    }
}
