﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoletaGaming.NewPlatformer2D
{
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(BoxCollider2D))]
    public class RotacionFlecha : MonoBehaviour
    {
        public bool shoot = false;

        void Update()
        {
            if (!shoot)
            {

                Vector3 diferencia = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
                diferencia.Normalize();
                float rotZ = Mathf.Atan2(diferencia.y, diferencia.x) * Mathf.Rad2Deg;
                transform.rotation = Quaternion.Euler(new Vector3(0, 0, rotZ));


            }
        }


    }

}
