﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoletaGaming.NewPlatformer2D
{
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(BoxCollider2D))]
    public class trap1 : MonoBehaviour
    {
        void OnCollisionEnter2D(Collision2D other)
        {
            switch (other.gameObject.tag)
            {

                case "Player":
                    Debug.Log("ENTRO EL PLAYER!!");
                    GetComponent<Animator>().SetBool("playerEnter", true);
                    break;

                default:

                    break;
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            switch (other.gameObject.tag)
            {

                case "Player":
                    GetComponent<Animator>().SetBool("playerEnter", true);
                    break;

                default:

                    break;
            }
        }

        public void AccessKillPlayer()
        {
            GetComponent<AudioSource>().Play();
            FindObjectOfType<GameManager>().KillPlayer();
        }
    }


}
