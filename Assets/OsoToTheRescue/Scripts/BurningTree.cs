﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
namespace BoletaGaming.NewPlatformer2D
{
    public class BurningTree : MonoBehaviour
    {
        [SerializeField] UnityEvent onTurnOff;
        public Sprite turnedOffTreeSprite;
        public bool turnedOff = false;
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void TurnOff()
        {
            Destroy(GetComponent<Animator>());
            GetComponent<SpriteRenderer>().sprite = turnedOffTreeSprite;
            turnedOff = true;
            onTurnOff.Invoke();
        }
    }
}

