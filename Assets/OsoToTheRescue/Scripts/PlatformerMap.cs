﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace BoletaGaming.NewPlatformer2D
{
    public class PlatformerMap : MonoBehaviour
    {
        public int numberOfTrees;
        public Transform burningTreeHolder;
        public Vector3 initialPlayerPosition;
        public GameObject player;
        // Start is called before the first frame update
        void Start()
        {
            FindObjectOfType<GameManager>().SetUp(burningTreeHolder, initialPlayerPosition, player, transform, numberOfTrees);
        }
    }

}
