﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoletaGaming.NewPlatformer2D
{

    public static class ExtensionTransform
    {
        public static void Clear(this Transform self)
        {
            foreach (Transform child in self)
            {
                GameObject.Destroy(child.gameObject);
            }
        }
    }

    public static class ExtensionGeneral
    {
        public static T GetRandom<T>(this IList<T> self)
        {
            if (self.Count == 0)
                return default(T);

            return self[Random.Range(0, self.Count)];
        }

        public static void Swap<T>(ref T a, ref T b)
        {
            T c = a;
            a = b;
            b = c;
        }

        public static IList<T> Shuffle<T>(this IList<T> self)
        {
            int len = self.Count;
            for (int ii = 0; ii < len; ii++)
            {
                T a = self[ii];
                T b = self[Random.Range(0, len)];
                Swap(ref a, ref b);
            }
            return self;
        }

        public static Vector3 AddFloat(this Vector3 self, float val)
        {
            self.x += val;
            self.y += val;
            self.z += val;
            return self;
        }
    }
}