﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace BoletaGaming.NewPlatformer2D
{

    public class Jugador : MonoBehaviour
    {

        protected Rigidbody2D rb;
        protected BoxCollider2D col;
        public int damage = 1;
        public float rapidez = 4;

        public Transform firePoint;
        public float arrowSpeed = 6;
        public GameObject arrowPrefab;
        public GameObject currentArrow;
        public float jumpForce = 6f;
        private float floorMinDistance;
        public Animator animator;
        public bool jumping = false;

        public AudioSource playerSound;
        public AudioClip playerJump;

        public bool dead;


        void Awake()
        {
            dead = false;
            rb = GetComponent<Rigidbody2D>();
            col = GetComponent<BoxCollider2D>();
            playerSound = GetComponent<AudioSource>();
            animator = GetComponent<Animator>();
            floorMinDistance = col.size.y / 2 + 0.2f;
        }

        void Update()
        {
            if (dead) return;

            if (rb.velocity.y > 0)
            {
                jumping = true;
            }
            
            float inputX = Input.GetAxisRaw("Horizontal");

            if (inputX < 0) GetComponent<SpriteRenderer>().flipX = true;
            else if (inputX > 0) GetComponent<SpriteRenderer>().flipX = false;

            animator.SetFloat("Speed", Mathf.Abs(inputX));

            if (Mathf.Abs(inputX) != 0)
            {
                rb.position += new Vector2(inputX, 0) * rapidez * Time.deltaTime;
            }

            Debug.DrawRay(transform.position, Vector3.down * floorMinDistance);

            if (!jumping && Input.GetKeyDown(KeyCode.Space) && Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y), Vector2.down, floorMinDistance))
            {
                playSound(playerJump);
                animator.SetBool("IsJumping", true);
                rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            }

            if(rb.velocity.y == 0)
            {
                jumping = false;
            }
        }

        void OnCollisionEnter2D(Collision2D other)
        {
            switch (other.gameObject.tag)
            {
                case "Indestructible":
                    animator.SetBool("IsJumping", false);
                    break;

                default:

                    break;
            }
        }

        public void IsDiying()
        {
            dead = true;
        }

        public void OnAnimationDeadEnd()
        {
            Destroy(gameObject);
            FindObjectOfType<GameManager>().RestartGame();
        }

        private void OnDestroy()
        {
            
        }


        void playSound(AudioClip sound)
        {
            playerSound.clip = sound;
            playerSound.Play();
        }

    }
}
