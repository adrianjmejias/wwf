﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace BoletaGaming.NewPlatformer2D
{
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(BoxCollider2D))]
    public abstract class Personaje : MonoBehaviour
    {
        protected Rigidbody2D rb;
        protected BoxCollider2D col;
        public int vida = 3;
        public int dano = 1;
        public float rapidez = 1;

        protected virtual void Awake()
        {
            rb = GetComponent<Rigidbody2D>();
            col = GetComponent<BoxCollider2D>();
        }

        public abstract void TomarDano(int dmg);
        public abstract void Morir();
    }

}
