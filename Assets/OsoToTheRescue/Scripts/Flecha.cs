﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoletaGaming.NewPlatformer2D
{

    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(BoxCollider2D))]
    public class Flecha : MonoBehaviour
    {

        [System.NonSerialized] public Rigidbody2D rb;
        [System.NonSerialized] public BoxCollider2D col;

        void Awake()
        {
            rb = GetComponent<Rigidbody2D>();
            col = GetComponent<BoxCollider2D>();
            Destroy(gameObject, 5);
        }


        void OnCollisionEnter2D(Collision2D other)
        {
            Debug.Log("flecha collision");
            switch (other.gameObject.tag)
            {
                case "Indestructible":
                    rb.velocity = Vector2.zero;
                    rb.simulated = false;
                    rb.angularVelocity = 0;
                    rb.bodyType = RigidbodyType2D.Static;
                    col.enabled = false;
                    Destroy(gameObject, 5);
                    break;
            }
        }
    }

}