﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace BoletaGaming.NewPlatformer2D
{
    public class RotacionArma : MonoBehaviour
    {

        public SpriteRenderer padreSpriteRenderer;

        void Update()
        {
            Vector3 diferencia = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            diferencia.Normalize();

            float rotZ = Mathf.Atan2(diferencia.y, diferencia.x) * Mathf.Rad2Deg;

            if (rotZ > 90 && rotZ < 180)
            {
                padreSpriteRenderer.flipX = true;
            }
            else
            {
                padreSpriteRenderer.flipX = false;
            }

            transform.rotation = Quaternion.Euler(new Vector3(0, 0, rotZ));

        }
    }
}

