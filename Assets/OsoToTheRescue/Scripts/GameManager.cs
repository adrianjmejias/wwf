﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BoletaGaming.WWF.Minigame;
using BoletaGaming.WWF;
using BoletaGaming.AudioGraph;

namespace BoletaGaming.NewPlatformer2D
{

    public class GameManager : Minigame
    {

        //referenes get by prefab
        public Transform gameHolder;
        public Transform burningTreeHolder;
        public Vector3 initialPlayerPosition;
        public GameObject currentPlayer;
        public int remainingTree = -1;

        public Transform mapHolder;
        public List<GameObject> mapPrefabs;
        public GameObject playerPrefab;
        public Camera gameCamera;
        public int playersLife = 3;
        public Rigidbody2D playerRb;
        bool mapSetUp = false;
        int currentLevel;


        public AudioNode playerSound;
        public AudioClipReference playerDie;
        public AudioClipReference playerTurnOff;
        public AudioClipReference winSound;
        public AudioClipReference loseSound;


        public Transform lifeHolder;


        public Text remainingTreesText;

        private void Start()
        {
            //MG_Start(0);
        }

        // Update is called once per frame
        void Update()
        {

            if (mapSetUp)
            {
                remainingTreesText.text = remainingTree.ToString();
                if (Input.GetKeyDown(KeyCode.F)) CheckCanSaveTree();

                if (!currentPlayer || currentPlayer.GetComponent<Jugador>().dead) return;

                if (playerRb.velocity.y < -50 && currentLevel == 2)
                {
                    KillPlayer();
                }
            }
            

        }        

        public void CheckCanSaveTree()
        {
            foreach (Transform child in burningTreeHolder)
            {
                if (Vector3.Distance(child.transform.position, currentPlayer.transform.position) < 3)
                {
                    BurningTree bt = child.GetComponent<BurningTree>();
                    if (!bt.turnedOff)
                    {
                        playerSound.PlaySFX(playerTurnOff);
                        bt.TurnOff();
                        remainingTree--;

                        if(remainingTree <= 0)
                        {
                            MG_End(true);
                        }
                    }
                }
            }
        }

        public void KillPlayer()
        {
            if (currentPlayer.GetComponent<Jugador>().dead) return;

            currentPlayer.GetComponent<Jugador>().IsDiying();
            currentPlayer.GetComponent<Animator>().SetBool("IsDead", true);
            Destroy( currentPlayer.GetComponent<Rigidbody2D>() );
            playerSound.PlaySFX(playerDie);
            
        }

        public void RestartGame()
        {

            if (playersLife > 0)
            {
                Loselife();
                if (playersLife > 0) SpawnPlayer();
                gameCamera.GetComponent<CameraFollow>().player = currentPlayer;
            }
            if (playersLife <= 0)
            {
                MG_End(false);
            }
        }


        public void Loselife()
        {
            playersLife--;

            if (playersLife == 2) // ??????????????????????????????????????????????????????????
            {
                lifeHolder.GetChild(0).gameObject.SetActive(false);
                lifeHolder.GetChild(1).gameObject.SetActive(true);
                lifeHolder.GetChild(2).gameObject.SetActive(true);
            }
            else if (playersLife == 1)
            {
                lifeHolder.GetChild(0).gameObject.SetActive(false);
                lifeHolder.GetChild(1).gameObject.SetActive(false);
                lifeHolder.GetChild(2).gameObject.SetActive(true);
            }
            else
            {
                lifeHolder.GetChild(0).gameObject.SetActive(false);
                lifeHolder.GetChild(1).gameObject.SetActive(false);
                lifeHolder.GetChild(2).gameObject.SetActive(false);
            }
        }


        public void SpawnPlayer()
        {
            currentPlayer = Instantiate(playerPrefab, gameHolder);
            playerRb = currentPlayer.GetComponent<Rigidbody2D>();
        }

        public override void MG_Reset()
        {
            throw new System.NotImplementedException();
        }

        public override void MG_Quit()
        {
            throw new System.NotImplementedException();
        }

        public override void MG_Pause()
        {
            throw new System.NotImplementedException();
        }

        public override void MG_Start(int _id)
        {
            mapHolder.Clear();
            currentLevel = _id;
            base.MG_Start(_id);
            gameCamera.depth = 100;
            playersLife = 3;
            Instantiate(mapPrefabs[_id],mapHolder);

            lifeHolder.GetChild(0).gameObject.SetActive(true);
            lifeHolder.GetChild(1).gameObject.SetActive(true);
            lifeHolder.GetChild(2).gameObject.SetActive(true);
        }


        public override void MG_End(bool _win)
        {
            base.MG_End(_win);

            if (_win)
            {
                playerSound.PlaySFX(winSound);
            }
            else
            {
                playerSound.PlaySFX(loseSound);
            }
            gameCamera.depth = -1;

            Destroy(gameHolder.gameObject);
            mapSetUp = false;
        }


        public void SetUp(Transform burningTreeHolder, Vector3 initialPlayerPosition, GameObject player, Transform gameHolder, int numberOfTrees)
        {
            this.gameHolder = gameHolder;
            this.burningTreeHolder = burningTreeHolder;
            this.initialPlayerPosition = initialPlayerPosition;
            currentPlayer = player;
            remainingTree = numberOfTrees;

            FindObjectOfType<CameraFollow>().player = currentPlayer;

            playerRb = currentPlayer.GetComponent<Rigidbody2D>();

            mapSetUp = true;
        }

        public override int GetNumLevels()
        {
            return mapPrefabs.Count - numMixLevels;
        }
    }
}