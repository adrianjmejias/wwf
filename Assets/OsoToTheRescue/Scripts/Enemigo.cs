﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace BoletaGaming.NewPlatformer2D
{
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(BoxCollider2D))]
    public class Enemigo : Personaje
    {

        new void Awake()
        {
            base.Awake();



        }

        void Update()
        {
            rb.velocity = transform.right.normalized * rapidez;
        }

        void OnCollisionEnter2D(Collision2D other)
        {
            switch (other.gameObject.tag)
            {
                case "Indestructible":
                    transform.Rotate(Vector3.up, 180f);
                    break;

                case "Player":
                    FindObjectOfType<GameManager>().KillPlayer();
                    break;
                case "Enemy":
                    transform.Rotate(Vector3.up, 180f);
                    break;


                default:

                    break;
            }
        }

        public override void TomarDano(int dmg)
        {
            vida -= dmg;
        }

        public override void Morir()
        {
            Destroy(gameObject);
        }
    }
}

