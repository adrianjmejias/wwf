﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoletaGaming.Memory
{
    [CreateAssetMenu(fileName = "Memory Level ", menuName = "Create Memory Level")]
    public class MemoryLevel : ScriptableObject
    {
        [NaughtyAttributes.InfoBox("Los sprites deben todos tener nombres distintos!!")]
        public Sprite[] pairImage;
        public float time;
        public SpriteReference backFace;
    }
}
