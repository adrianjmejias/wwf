using BoletaGaming.WWF.Minigame;
using BoletaGaming.WWF;
using BoletaGaming.AudioGraph;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using NaughtyAttributes;

namespace BoletaGaming.Memory
{

    public class Memory : Minigame
    {
        [SerializeField] Text textTimer;
        public List<Sprite> spritePool;
        int numTokens = 18;
        private const int MAX_FLIPPED = 2;
        private static Memory PInstance = null;
        public static Memory Instance
        {
            get
            {
                return PInstance;
            }
        }

        [SerializeField] Timer timer;
        [BoxGroup("UI")] public HolderCombo.HolderCombo comboPairs;
        [BoxGroup("UI")] public HolderCombo.HolderCombo comboTargets;
        [BoxGroup("UI")] public float timeShowTokens = 1;

        [BoxGroup("Database")] public MemoryLevel[] levels;

        [BoxGroup("Sound")] public AudioNode gameSound;
        [BoxGroup("Sound")] public SFX sfx;

        [BoxGroup("State")] public List<MemoryToken> memoryTokens = new List<MemoryToken>();
        [BoxGroup("State")] public List<MemoryToken> flipped = new List<MemoryToken>();
        [BoxGroup("State")] public MemoryLevel actLevel;
        [BoxGroup("State")] public int index = 0;
        [BoxGroup("State")] public int correctCount = 0;

        public void UpdateTime()
        {
            System.TimeSpan t = System.TimeSpan.FromSeconds(timer.timeLeft);


            textTimer.text =

                string.Format("{0:D2}m:{1:D2}s",
                    t.Minutes,
                    t.Seconds
                );


            timer.timeLeft.ToString();
        }

        public override void MG_Pause()
        {
            throw new System.NotImplementedException();
        }

        public override void MG_Quit()
        {
            throw new System.NotImplementedException();
        }

        public override void MG_Reset()
        {
            throw new System.NotImplementedException();
        }

        public override void MG_End(bool _win)
        {
            base.MG_End(_win);
        }

        private void Awake()
        {
            if (!Instance)
            {
                PInstance = this;
            }
        }


        public override void MG_Start(int _id)
        {
            base.MG_Start(_id);
            actLevel = levels[_id];
            SetUI(actLevel);
            timer.StartTimer(actLevel.time, 0.1f);
        }

        public void FlipToken(MemoryToken _token, float time)
        {
            StartCoroutine(CR_Flip(_token, time));
        }

        private System.Collections.IEnumerator CR_Flip(MemoryToken _token, float time)
        {
            if (flipped.Count >= MAX_FLIPPED)
            {
                gameSound.PlaySFX(sfx.PossiblePressFail);
                yield break; //salir de corutina pq no podemos flippear
            }

            if(_token.flipped)
            {
                gameSound.PlaySFX(sfx.SolutionPressFail);
                yield break;
            }

            // en este punto sabemos que 
            // --- Se pueden voltar tokens
            // --- El tokken seleccionado est� boca abajo


            _token.flipped = !_token.flipped;
            flipped.Add(_token);
            _token.Flip(true);
            gameSound.PlaySFX(sfx.PossiblePressOk);
            

            if (flipped.Count >= MAX_FLIPPED) // ya tengo 2
            {

                bool _win = flipped.TrueForAll(x => x.name == flipped[0].name);
                yield return new WaitForSecondsRealtime(time);
                if (_win)
                {
                    gameSound.PlaySFX(sfx.SolutionPressOk);
                    //Debug.Log("win");
                    flipped.ForEach(x => { x.gameObject.SetActive(false); }); flipped.Clear();
                    correctCount++;


                    if (correctCount == numTokens)//actlevel.fichas.count estaba antes
                    {
                        MG_End(true);
                    }
                }
                else
                {
                    //Debug.Log("loose");
                    gameSound.PlaySFX(sfx.SolutionPressFail);

                    flipped.ForEach(x => { x.Flip(false); }); flipped.Clear();
                }
            }
        }
        public void SetUI(MemoryLevel value)
        {
            correctCount = 0;
            comboPairs.Clear();
            comboTargets.Clear();
            memoryTokens.Clear();

            List<Sprite> _pairs = new List<Sprite>();

            spritePool.Shuffle().Shuffle();


            _pairs.AddRange(spritePool.Take(numTokens));
            for(int ii= 0, iiEnd = _pairs.Count*2;ii< iiEnd; ii+=2)
            {
                _pairs.Insert(ii + 1, _pairs[ii]);
            }

            for(int ii= 0; ii<_pairs.Count; ii++)
            {
                _pairs.ShuffleRange(ii, ii + 6);
            }

            foreach (Sprite _pair in _pairs)
            {
                MemoryToken _p = comboPairs.Get().GetComponent<MemoryToken>();
                memoryTokens.Add(_p.Init(_pair.name, _pair, value.backFace.value, comboTargets.Get().transform, () =>
                 {
                     FlipToken(_p, timeShowTokens);
                 }));
                _p.Flip(true);
            }


            float init = 3, delta = 0.3f;
            memoryTokens.ForEach(x =>
            {
                StartCoroutine(Helpers.CallAfterSeconds(init += delta, () =>
                  {
                      //Debug.Log("flipping " + x.name);
                      x.Flip(false);
                  })
                );
            });
        }

        public override int GetNumLevels()
        {
            return levels.Length - numMixLevels;
        }
    }

    [System.Serializable]
    public class Sound
    {
        [SerializeField] private AudioSource audioSource;
        [SerializeField] private AudioClip[] SFXs;
        [SerializeField] private AudioClip[] BGs;


        public Sound PlaySFX(SFX sfx)
        {
            // audioSource.clip = SFXs[(int)sfx];
            // audioSource.Play();
            return this;
        }
    }
    [System.Serializable]
    public class SFX
    {
        public AudioClipReference PossiblePressOk;
        public AudioClipReference PossiblePressFail;
        public AudioClipReference SolutionPressOk;
        public AudioClipReference SolutionPressFail;
        public AudioClipReference Win;
        public AudioClipReference Loose;
    }

    public enum BG
    {

    }
}

