﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


namespace BoletaGaming.Memory
{
    [RequireComponent(typeof(SmoothFollow))]
    [RequireComponent(typeof(Animator))]
    public class MemoryToken : MonoBehaviour
    {
        [SerializeField] SmoothFollow smoothFollow;
        [SerializeField] private Animator animator;
        public Image backFace;
        public Image frontFace;
        public Button button;
        public bool flipped = true;
        public MemoryToken Init(string name, Sprite backFace, Sprite frontFace, Transform target, UnityAction action)
        {
            this.name = name;
            this.backFace.sprite = backFace;
            this.frontFace.sprite = frontFace;
            button.onClick.AddListener(action);
            smoothFollow.target = target;
            return this;
        }

        public MemoryToken Flip(bool _flip)
        {
            Debug.Log("flipping " + name);
            flipped = _flip;
            animator.SetBool("flipped", _flip);
            return this;
        }

        private void OnDisable()
        {
            if (smoothFollow.target)
            {
                smoothFollow.target.gameObject.SetActive(false);
            }
        }

        private void OnEnable()
        {
            if (smoothFollow.target)
            {
                smoothFollow.target.gameObject.SetActive(true);
            }

        }
    }
}
