﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BoletaGaming.WWF.Minigame;
using BoletaGaming.WWF;


namespace BoletaGaming.NewPuzzle
{
    public class NewPuzzle : Minigame
    {
        public Timer timer;
        [SerializeField] Text textTimer;

        public AudioClipReference win;
        public AudioClipReference lose;
        public BoletaGaming.AudioGraph.AudioNode audioNode;



        public Transform partHolder;
        public Transform puzzleHolder;
        public List<GameObject> LevelsPrefab;



        public void UpdateTime()
        {
            System.TimeSpan t = System.TimeSpan.FromSeconds(timer.timeLeft);


            textTimer.text =

                string.Format("{0:D2}m:{1:D2}s",
                    t.Minutes,
                    t.Seconds
                );


            timer.timeLeft.ToString();
        }
        public override void MG_End(bool _win)
        {
            base.MG_End(_win);
        }

        public override void MG_Pause()
        {

        }

        public override void MG_Quit()
        {

        }

        public override void MG_Reset()
        {

        }

        public override void MG_Start(int _id)
        {
            base.MG_Start(_id);
            puzzleHolder.Clear();
            GameObject go = Instantiate(LevelsPrefab[_id], puzzleHolder);
            partHolder = go.GetComponent<PuzzleSilhouette>().partHolder;
            timer.StartTimer(60*5, 0.1f);
        }



        // Update is called once per frame 
        void Update()
        {
            if (!partHolder) return;

            if (checkWin())
            {
                MG_End(true);
            }
        }

        public bool checkWin()
        {
            for (int i = 0; i < partHolder.childCount; i++)
            {
                Transform child = partHolder.GetChild(i);
                PuzzlePart pp = child.GetComponent<PuzzlePart>();
                if (pp.transform.localPosition != new Vector3(pp.correctPosX, pp.correctPosY, pp.correctPosZ)) return false;
            }
            Debug.Log("You win");
            return true;
        }

        public override int GetNumLevels()
        {
            return LevelsPrefab.Count - numMixLevels;
        }
    }
}

