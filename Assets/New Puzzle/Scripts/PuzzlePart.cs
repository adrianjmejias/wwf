﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


namespace BoletaGaming.NewPuzzle
{
    [RequireComponent(typeof(AudioSource))]
    public class PuzzlePart : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IDropHandler
    {

        public static GameObject itemBeingDragged;
        Vector3 startPos;

        public float correctPosX;
        public float correctPosY;
        public float correctPosZ;

        private float dropSensitivity = 50;

        private AudioSource partSound;
        public AudioClip soundBeginDrag;
        public AudioClip soundEndDrag;


        public void OnBeginDrag(PointerEventData eventData)
        {
            GetComponent<Image>().raycastTarget = false;
            Debug.Log("OnBeginDrag");
            itemBeingDragged = gameObject;
            startPos = transform.position;

        }

        public void OnDrag(PointerEventData eventData)
        {
            // Debug.Log("OnDrag");

            transform.position = Input.mousePosition;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            Debug.Log("OnEndDrag");

            GetComponent<Image>().raycastTarget = true;
            itemBeingDragged = null;

            Vector3 correctPos = new Vector3(correctPosX, correctPosY, correctPosZ);
            Vector3 currentPos = transform.localPosition;

            if (Mathf.Abs(correctPos.x - currentPos.x) < dropSensitivity && Mathf.Abs(correctPos.y - currentPos.y) < dropSensitivity && Mathf.Abs(correctPos.z - currentPos.z) < dropSensitivity)
            {
                Debug.Log("asdasdasd");
                transform.localPosition = correctPos;
            }
            else
            {
                transform.position = startPos;
            }
        }

        // Use this for initialization
        void Start()
        {

            partSound = GetComponent<AudioSource>();

            //NewPuzzle np = FindObjectOfType<NewPuzzle>();
            //transform.position = np.silhouetteTransform.position;

        }

        void playSound(AudioClip sound)
        {
            partSound.clip = sound;
            partSound.Play();
        }

        public void OnDrop(PointerEventData eventData)
        {
            
        }
    }
}
