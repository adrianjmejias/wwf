﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsFollow : MonoBehaviour
{
    Queue<Vector2> points = new Queue<Vector2>();
    public Transform target;
    public float samplingRate = 0.7f;
    public float toleranceDistance = 0.3f;
    public float t = 0.3f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Update()
    {
        int numPoints = points.Count;

        if (numPoints > 0)
        {
            Vector2 follow = points.Peek();

            transform.LookAt(follow, Vector3.back);
            transform.position = Vector3.Lerp(transform.position, follow,  t* points.Count);

            float distance = Vector3.Distance(transform.position, follow);

            if (distance < toleranceDistance)
            {
                points.Dequeue();
            }
        }
    }

    private void OnDrawGizmos()
    {
        foreach (var item in points)
        {
            Gizmos.DrawSphere(item, 0.8f);
        }
    }

    // Update is called once per frame
    private void OnEnable()
    {
        points.Clear();

        InvokeRepeating("Sample", 0, samplingRate);
    }

    private void OnDisable()
    {
        CancelInvoke();
    }

    private void Sample()
    {
        points.Enqueue(target.position);
    }
}
