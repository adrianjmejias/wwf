﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using BoletaGaming.AudioGraph;


namespace BoletaGaming.WWF
{

    public interface ShuffleableMinigame
    {
        void Shuffle();
    }

    public class GameManager : MonoBehaviour
    {
        [SerializeField] UnityEvent onLogin;
        private static GameManager PInstance = null;
        public static GameManager Instance
        {
            get
            {
                return PInstance;
            }
        }

        [NaughtyAttributes.MinMaxSlider(0.1f, 3)] [SerializeField] Vector2 mapZoom = new Vector2(0.5f, 2);
        [SerializeField] float zoomFactor = 0.25f;

        [SerializeField] private AudioNode audioController;
        [SerializeField] private RectTransform map;
        [SerializeField] GameObject buttonNext;

        public Database gameDatabase;
        public SFX sfxs;
        public UI gameUI;

        [SerializeField] IntReference playerScore;
        [SerializeField] Text playerScoreText;

        [SerializeField] StringReference playerName;
        [SerializeField] Text playerNameText;

        [SerializeField] InputField explorerName;
        [SerializeField] HolderCombo.HolderCombo comboHighScore;

        const string PP_VOL_SFX = "VOL_SFX", PP_VOL_BGM = "VOL_BGM";
        [SerializeField] Slider sliderVolSFX;
        [SerializeField] Slider sliderVolBGM;

        private void Update()
        {

            if (!gameDatabase.actMinigame)
            {
                //if (Input.GetMouseButtonDown(0))
                //{
                //    lastMousePos = Input.mousePosition;
                //}
                //if (Input.GetMouseButton(0))
                //{
                //    Vector2 nextMousePosition = Input.mousePosition;
                //    Vector2 dir = nextMousePosition - lastMousePos;
                //    lastMousePos = nextMousePosition;


                //    var nextPos = map.anchoredPosition;
                //    nextPos.x += dir.x;
                //    nextPos.y += dir.y;
                //    map.anchoredPosition = nextPos;
                //}

                float scroll = Input.GetAxis("Mouse ScrollWheel");
                if (scroll != 0)
                {
                    map.localScale = (map.localScale + map.localScale * scroll * 0.10f).Clamp(mapZoom.x, mapZoom.y);
                }


                if (Input.GetKey(KeyCode.Plus) || Input.GetKey(KeyCode.KeypadPlus))
                {
                    map.localScale = (map.localScale + map.localScale * zoomFactor * 0.10f).Clamp(mapZoom.x, mapZoom.y);


                }
                if (Input.GetKey(KeyCode.Minus) || Input.GetKey(KeyCode.KeypadMinus))
                {
                    map.localScale = (map.localScale - map.localScale * zoomFactor * 0.10f).Clamp(mapZoom.x, mapZoom.y);
                }

            }
            else
            {
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    BackToMap();
                }
            }
        }

        private void Awake()
        {
            if (!Instance)
            {
                PInstance = this;
            }
        }

        private void Start()
        {
            AudioNode.Instance = audioController; // asigno un master para que todos hagan los sonidos con este y poder controlar mejor todos los volumenes
            gameUI.Start();

            audioController.PlayBGMS(sfxs.Bgms);

            playerScore.value = GetOverallScore(playerName.value);

            sliderVolBGM.value = PlayerPrefs.GetFloat(PP_VOL_BGM, 1);
            sliderVolSFX.value = PlayerPrefs.GetFloat(PP_VOL_SFX, 1);
        }


        public void SetBGM(float val)
        {
            PlayerPrefs.SetFloat(PP_VOL_BGM, val);
        }
        public void SetSFX(float val)
        {
            PlayerPrefs.SetFloat(PP_VOL_SFX, val);
        }


        /// <summary>
        /// Comienza a jugar un punto de interés
        /// </summary>
        /// <param name="_poi">El punto de interés clickeado</param>
        /// <param name="_index">El juego que queremos jugar</param>
        public void StartPOI(POI _poi, int _index)
        {
            gameUI.MenuLevels.SetActive(false);
            PlayMinigame(_poi, _index);
        }

        /// <summary>
        /// Ocurre una vez terminas un juego
        /// </summary>
        /// <param name="_win">indica si se ganó el juego</param>
        public void EndMinigame(bool _win)
        {
            gameUI.SetActive(true);
            if (_win)
            {
                buttonNext.gameObject.SetActive((gameDatabase.indexMinigame + 1) < gameDatabase.actPOI.minigames.Count);

                gameUI.popupManager.SetPopup(PopupType.Win);
            }
            else
            {
                gameUI.popupManager.SetPopup(PopupType.Loose);
            }
        }
        public void PlayMinigame() { PlayMinigame(gameDatabase.actPOI, gameDatabase.indexMinigame); }
        public void PlayMinigame(POI _poi, int _index)
        {

            Screen.orientation = ScreenOrientation.Portrait;

            gameUI.popupManager.SetPopup(popup: PopupType.Win, active: false);
            gameUI.popupManager.SetPopup(popup: PopupType.Loose, active: false);

            var _gameID = _poi.minigames[_index];
            gameDatabase.actMinigame = gameDatabase.minigameMetas[(int)_gameID.gameType].minigame;
            gameDatabase.actPOI = _poi;
            gameDatabase.indexMinigame = _index;

            gameUI.SetActive(false);

            gameDatabase.actMinigame.MG_Start(_gameID.levelID);
        }
        public void NextMinigame()
        {
            gameUI.popupManager.SetPopup(PopupType.All, false);

            gameDatabase.actMinigame.gameObject.SetActive(false);

            gameDatabase.indexMinigame += 1;

            if (gameDatabase.indexMinigame < gameDatabase.actPOI.minigames.Count)
            {
                PlayMinigame();

            }
            else
            {
                Debug.Log("No hay más juegos");
                BackToMap();
            }
        }
        
        public void BackToMap()
        {
            playerScoreText.text = playerScore.value.ToString();
            var mini = gameDatabase.actMinigame;
            if (mini)
            {
                mini.gameObject.SetActive(false);
            }
            gameUI.SetActive(true);
            gameUI.popupManager.SetPopup(PopupType.All, false);
            gameDatabase.actPOI.CheckUnLockPOI();
            gameDatabase.actMinigame = null;
            gameDatabase.actPOI = null;
        }


        public class Player
        {
            public string name;
            public int score;
            public Player(string name, int score)
            {
                this.name = name;
                this.score = score;
            }
        }

        public void DeleteAllProgress()
        {
            ListString names = JsonUtility.FromJson<ListString>(PlayerPrefs.GetString(PP_PLAYER_NAMES, "{}"));

            names.list.ForEach(name => {
                foreach (var item in gameDatabase.minigameMetas)
                {
                    item.minigame.DeleteProgress(name);
                }
            });

            PlayerPrefs.DeleteKey(PP_PLAYER_NAMES);
        }

        public void FillHighScore()
        {

            ListString names = JsonUtility.FromJson<ListString>(PlayerPrefs.GetString(PP_PLAYER_NAMES, "{}"));
            List<Player> players = new List<Player>();

            names.list.ForEach(name =>
            {
                players.Add(new Player(name, GetOverallScore(name)));
            });

            comboHighScore.Clear();

            players.Sort((p1,p2)=> { return (p1.score< p2.score) ? 1 : -1; });

            players.ForEach(player =>
            {
                FieldHighScore field = comboHighScore.Get<FieldHighScore>();
                field.Set(player.name, player.score);
            });
        }

        private int GetOverallScore(string name)
        {
            int score = 0;
            foreach (var minigame in gameDatabase.minigameMetas)
            {
                score += minigame.minigame.ScoreForPlayer(name);
            }
            return score;
        }

        public class ListString{
            public List<string> list = new List<string>();
        }

        public const string PP_PLAYER_NAMES = "playerNames";
        //public string playerName = "defaultPlayerName";
        public void Login(InputField input)
        {
            playerName.value = explorerName.text;
            ListString names = JsonUtility.FromJson<ListString>(PlayerPrefs.GetString(PP_PLAYER_NAMES, "{}"));

            if (!names.list.Contains(playerName.value))
            {
                names.list.Add(playerName.value);
                PlayerPrefs.SetString(PP_PLAYER_NAMES, JsonUtility.ToJson(names));
            }


            playerNameText.text = playerName.value;
            playerScore.value = GetOverallScore(playerName.value);
            playerScoreText.text = playerScore.value.ToString();
            onLogin.Invoke();
        }
    }

    [System.Serializable]
    public class Database
    {
        public Minigame.Minigame actMinigame = null;
        public MinigameMeta[] minigameMetas;

        public int indexMinigame;
        public POI actPOI;
    }
    [System.Serializable]
    public class MinigameMeta
    {
        public string name;
        private Minigame.Minigame _minigame;
        public Minigame.Minigame minigame {
            get {
                if (!_minigame)
                {
                    _minigame = GameObject.Instantiate(prefab, GameManager.Instance.transform).GetComponent<Minigame.Minigame>();
                    _minigame.gameObject.SetActive(false);
                    //ShuffleableMinigame _shuffleable = _minigame as ShuffleableMinigame;
                    //if (_shuffleable != null)
                    //{
                    //    _shuffleable.Shuffle();
                    //}
                }


                return _minigame;
            }
        }
        public GameObject prefab;
    }

    [System.Serializable]
    public class UI
    {
        public Canvas mapCanvas;
        public GameObject MenuLevels;
        public PopupManager popupManager;
        public Sprite[] verticalBackgrounds;

        public UI Start()
        {
            popupManager.SetPopup(PopupType.All, false);
            return this;
        }

        public UI SetActive(bool active)
        {

            if (mapCanvas)
            {
                mapCanvas.gameObject.SetActive(active);
            }

            return this;
        }


        public void SaveGame()
        {
            //PPWrapper
        }
        public void LoadGame()
        {

        }
    }

    //public class WWF_SaveGame{
    //    public string playerName;
    //}

    [System.Serializable]
    public class SFX
    {
        public List<AudioClip> Bgms;
    }
}
