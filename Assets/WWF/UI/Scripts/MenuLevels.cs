﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BoletaGaming.WWF
{
    public class MenuLevels : MonoBehaviour
    {
        [SerializeField] HolderCombo.HolderCombo levelsInstantiation;
        [SerializeField] Text gameName;
        [SerializeField] Text gameProgress;
    
        public void SetUp(IList<MinigameID> metadatas, IList<bool> completion)
        {
            for (int ii = 0; ii < metadatas.Count; ii++)
            {
                GameObject go = levelsInstantiation.Get();

                Image[] im = go.GetComponentsInChildren<Image>();

                im[1].gameObject.SetActive(completion[ii]);
            }
            gameObject.SetActive(true);
        }
    }
}
