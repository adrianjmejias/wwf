﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomSlider : Slider
{
    public void SetTextPercentage(Text target)
    {
        target.text = ((int)(normalizedValue * 100)).ToString() +"%";
    }
}
