﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LinkButton : Button
{
    [SerializeField] string _url;
    public string url
    {
        get
        {
            return _url;
        }
        set
        {
            _url = value;
        }
    }

    protected override void Start()
    {
        base.Start();
        onClick.AddListener(() => { Application.OpenURL(_url); });
    }
}
