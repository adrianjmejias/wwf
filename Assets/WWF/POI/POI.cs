﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using BoletaGaming.AudioGraph;
namespace BoletaGaming.WWF
{
    public class POI : MonoBehaviour
    {
        [SerializeField] Animator animator;
        [SerializeField] Sprite vainaQueVoyACablear;
        [SerializeField] float percentageToUnlock = 0.5f;
        public GameType overrideGameType;
        public Button button;
        public bool interactable
        {
            get
            {
                return button.interactable;
            }
            set
            {
                bool newVal = value;
                bool oldVal = interactable;

                if(newVal && newVal != oldVal)
                {
                    animator.SetTrigger("blocked");    
                }
                oldVal = newVal;
                Debug.Log("Setting " + name + " blocked " + value.ToString());


                button.interactable = newVal;
            }
        }
        [NaughtyAttributes.Button]
        public void ToggleState()
        {
            interactable = !interactable;
        }
        public UnityEvent OnClickCallback;
        public Text menuGamesName;
        [System.NonSerialized]
        public List<MinigameID> minigames = new List<MinigameID>();
        public POI[] nextLevels;
        [SerializeField] Text progressText;
        [SerializeField] HolderCombo.HolderCombo levelsInstantiation;
        [SerializeField] AudioNode audioNode;
        [SerializeField] AudioClipReference levelClick;
        [SerializeField] AudioClipReference pOIClick;
        [SerializeField] AudioClipReference poiUnLock;
        public void ChangeSprite()
        {
            StartCoroutine(Helpers.CallAfterSeconds(2, () =>
            {
                button.GetComponent<Image>().sprite = vainaQueVoyACablear;
                audioNode.PlaySFX(poiUnLock);
                Debug.Log("cambiando sprite");

            }));
        }

        public void OnLogin()
        {
            CheckUnLockPOI();
        }

        public void OnClick()
        {
            //GameManager.Instance.gameDatabase


            if (overrideGameType != GameType.None)
            {
                var mini = GameManager.Instance.gameDatabase.minigameMetas[(int)overrideGameType].minigame;
                mini.Load();
                int numLevels = mini.GetNumLevels();
                int numLevelsCompleted = mini.completed.list.Count;

                if (minigames.Count <= 0)
                {
                    foreach (MinigameID item in minigames)
                    {
                        item.gameType = overrideGameType;
                    }
                    for (int ii = 0; ii < numLevels; ii++)
                    {
                        minigames.Add(new MinigameID(overrideGameType, ii));
                    }
                }
                //CheckUnLockPOI(numLevelsCompleted / (float)numLevels, percentageToUnlock);

                progressText.gameObject.SetActive(true);

                progressText.text = numLevelsCompleted.ToString() + "/" + numLevels.ToString();
            }
            else
            {
                progressText.gameObject.SetActive(false);
            }
            //Debug.Log("clicking");
            levelsInstantiation.Clear();
            audioNode.PlaySFX(pOIClick);

            if (overrideGameType != GameType.None)
            {
                MinigameMeta meta = GameManager.Instance.gameDatabase.minigameMetas[(int)overrideGameType];
                menuGamesName.text = meta.name;
            }
            else
            {
                menuGamesName.text = "Seleccione nivel";
            }

            List<Image> listonsitos = new List<Image>();
            //List<bool> completedLevels = meta.minigame.GetCompletedLevels();
            for (int _ii = 0; _ii < minigames.Count; _ii++)
            {
                MinigameID _mg = minigames[_ii];
                GameObject _lv = levelsInstantiation.Get();

                Text _tx = _lv.GetComponentInChildren<Text>();
                Button _bt = _lv.GetComponentInChildren<Button>();
                Image _finished = _lv.GetComponentsInChildren<Image>()[1];
                listonsitos.Add(_finished);
                //Image _im = _lv.GetComponentInChildren<Image>();

                _tx.text = _mg.levelID.ToString();
                int _closureii = _ii;
                _bt.onClick.AddListener(() =>
                {
                    LevelClick(_closureii);
                });
                //int _minigameID = (int)_mg.gameType;
            }
            listonsitos.ForEach(x => x.gameObject.SetActive(false));

            if (overrideGameType != GameType.None)
            {
                var completed = GameManager.Instance.gameDatabase.minigameMetas[(int)overrideGameType].minigame.completed.list;

                completed.ForEach(idx =>
                {
                    listonsitos[idx].gameObject.SetActive(true);
                });
            }

            OnClickCallback.Invoke();
        }
        [NaughtyAttributes.Button]
        public void CheckUnLockPOI()
        {
            if (overrideGameType != GameType.None)
            {
                var mini = GameManager.Instance.gameDatabase.minigameMetas[(int)overrideGameType].minigame;
                mini.Load();
                int numLevels = mini.GetNumLevels();
                int numLevelsCompleted = mini.completed.list.Count;
                CheckUnLockPOI(numLevelsCompleted / (float)numLevels, percentageToUnlock);
            }
        }
        public void CheckUnLockPOI(float act, float needed)
        {
            bool interactable = act >= needed;

            foreach (var item in nextLevels)
            {
                item.interactable = interactable;
            }
        }

        public void LevelClick(int _index)
        {
            audioNode.PlaySFX(levelClick);
            GameManager.Instance.StartPOI(this, _index);
        }
    }


}