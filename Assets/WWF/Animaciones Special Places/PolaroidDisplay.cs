﻿using System.Collections;
using System.Collections.Generic;
using BoletaGaming.AudioGraph;
using UnityEngine;
using UnityEngine.UI;



public class PolaroidDisplay : MonoBehaviour
{
    [SerializeField] Image image;
    [SerializeField] SpecialPlace act;
    [SerializeField] AudioNode sound;
    //[SerializeField] VRPlayer player;

    public void SetSpecialPlace(SpecialPlace specialPlace)
    {
        act = specialPlace;
        image.sprite = act.polaroid;
        gameObject.SetActive(true);
        sound.PlaySFX(specialPlace.clickSound);
        gameObject.SetActive(false);
    }

    public void Play360()
    {
        //player.Play(act.video);
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            
            //player.gameObject.SetActive(false);
        }
    }
}
