﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class SpecialPlace : MonoBehaviour
{
    [SerializeField] public Sprite polaroid;
    [SerializeField] public VideoClip video;
    [SerializeField] public AudioClip clickSound;
}
