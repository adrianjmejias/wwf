﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoletaGaming.WWF
{
    public enum PopupType
    {
        Win,
        Loose,
        All,
    }

    public class PopupManager : MonoBehaviour
    {
        public static PopupManager Instance = null;
        public RectTransform[] popups;

        private void Awake()
        {

            Instance = this;
        }


        public void SetPopup(PopupType popup, bool active = true)
        {
            if (popup == PopupType.All)
            {
                for (int ii = 0; ii < popups.Length; ii++)
                {
                    popups[ii].gameObject.SetActive(active);
                }
            }
            else
            {
                popups[(int)popup].gameObject.SetActive(active);
            }
        }

    }
}

