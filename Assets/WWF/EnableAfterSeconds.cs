﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoletaGaming
{
    public class EnableAfterSeconds : MonoBehaviour
    {
        [SerializeField] float seconds;
        private void Awake()
        {
            Invoke("Enable", seconds); ;
        }

        private void Enable()
        {
            gameObject.SetActive(true);
        }
    }
}
