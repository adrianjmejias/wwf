﻿using System.Collections.Generic;
using UnityEngine;
using BoletaGaming.WWF;
namespace BoletaGaming.WWF.Minigame
{
    [System.Serializable]
    public abstract class Minigame : MonoBehaviour
    {
        [System.Serializable]
        public class ListInt { public List<int> list  = new List<int>();}

        protected int _indexGame = 0;

        public ListInt completed = new ListInt();
        [SerializeField] protected StringReference playerName;
        [SerializeField] protected IntReference playerScore;
        public int numMixLevels = 0;
        public int ScorePerGame = 100;


        public abstract void MG_Reset();
        public abstract void MG_Quit();
        public abstract void MG_Pause();

        public virtual void MG_Start(int _id)
        {
            Load();
            _indexGame = _id;
            gameObject.SetActive(true);
        }
        public virtual void MG_End(bool _win)
        {
            if (_win && !completed.list.Contains(_indexGame))
            {
                playerScore.value += ScorePerGame;
                
                completed.list.Add(_indexGame);
                Save();
            }
            //Debug.Log("MG_End " + _win.ToString());
            gameObject.SetActive(false);


            var WWF_Manager = GameManager.Instance;
            if (WWF_Manager)
            {
                WWF_Manager.EndMinigame(_win);
            }
        }


        private string GAME_KEY(string playerName)
        {
           return this.GetType().Name + "," + playerName;
        }
        public virtual void Load()
        {
            completed = JsonUtility.FromJson<ListInt>(PlayerPrefs.GetString(GAME_KEY(playerName.value), "{}"));
        }
        public virtual ListInt Load(string name)
        {
            return JsonUtility.FromJson<ListInt>(PlayerPrefs.GetString(GAME_KEY(name), "{}"));

        }
        public void DeleteProgress(string name)
        {
            PlayerPrefs.DeleteKey(GAME_KEY(name));
        }
        public int ScoreForPlayer(string name)
        {
            ListInt lint = Load(name);
            return lint.list.Count * ScorePerGame;
        }

        public virtual void Save()
        {
            PlayerPrefs.SetString(GAME_KEY(playerName.value), JsonUtility.ToJson(completed));
        }
        public abstract int GetNumLevels();

        protected virtual void Awake()
        {
            Load();
        }
    }
}
