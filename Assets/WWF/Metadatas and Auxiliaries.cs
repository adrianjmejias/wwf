﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoletaGaming.WWF
{
    public enum GameType
    {
        TriviaQuestions,
        TriviaImages,
        Pacman,
        Reciclaje,
        Memory,
        FindDifferences,
        HiddenObjects,
        Platformer,
        Puzzle,
        Mixed,
        None,
    }

    [System.Serializable]
    public class MinigameID
    {
        public GameType gameType;
        public int levelID;
        public MinigameID(GameType gameType, int levelID)
        {
            this.gameType = gameType;
            this.levelID = levelID;
        }
    }


}

namespace BoletaGaming
{
    public static class Helpers
    {
        public static IEnumerator CallAfterSeconds(float time, System.Action callback)
        {
            yield return new WaitForSeconds(time);
            callback();
        }

    }
}