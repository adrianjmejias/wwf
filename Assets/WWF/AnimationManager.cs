﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

namespace BoletaGaming.WWF
{
    public class AnimationManager : MonoBehaviour
    {
        [ReorderableList][SerializeField] List<GameObject> telefericos;
        // Start is called before the first frame update
        private void OnEnable()
        {
            telefericos.ForEach(x=>x.SetActive(false));

            float init = 0, delta = 3f;
            foreach (GameObject tele in telefericos)
            {

                StartCoroutine(Helpers.CallAfterSeconds(init +0, () => tele.SetActive(true)));
                init += delta;
            }
        }

    }

}
