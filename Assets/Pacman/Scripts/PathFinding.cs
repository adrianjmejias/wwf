﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoletaGaming.Pacman
{
    public class PathFinding : MonoBehaviour
    {

        public Transform seeker, target;

        private Node[,] grid;


        public List<Node> path;

        private void Start()
        {

        }

        void Update()
        {


        }

        public void CreateGridFromMap(int[,] map)
        {

            grid = new Node[map.GetLength(0), map.GetLength(1)];

            for (int x = 0; x < map.GetLength(0); x++)
            {
                for (int y = 0; y < map.GetLength(1); y++)
                {
                    //for portals
                    if(x == 0 || x == map.GetLength(0) - 1)
                    {
                        if(y == 14 || y == 9)
                        {
                            grid[x, y] = new Node(false, x, y);
                            continue;
                        }
                       
                    }

                    if (map[x, y] == 1) grid[x, y] = new Node(false, x, y);
                    else grid[x, y] = new Node(true, x, y);

                }
            }
        }

        public void FindPath(Vector3 startPos, Vector3 targetPos)
        {

            Node startNode = grid[(int)startPos.x, (int)startPos.y];
            Node targetNode = grid[(int)targetPos.x, (int)targetPos.y];

            List<Node> openSet = new List<Node>();
            HashSet<Node> closedSet = new HashSet<Node>();
            openSet.Add(startNode);

            while (openSet.Count > 0)
            {

                Node currentNode = openSet[0];
                for (int i = 1; i < openSet.Count; i++)
                {
                    if (openSet[i].fCost < currentNode.fCost || openSet[i].fCost == currentNode.fCost && openSet[i].hCost < currentNode.hCost)
                    {
                        currentNode = openSet[i];
                    }
                }

                openSet.Remove(currentNode);
                closedSet.Add(currentNode);

                if (currentNode == targetNode)
                {
                    RetracePath(startNode, currentNode);
                    return;
                }


                //Debug.Log(currentNode.gridX);

                Node[] neighbours = new Node[4];
                neighbours[0] = grid[currentNode.gridX + 1, currentNode.gridY];
                neighbours[1] = grid[currentNode.gridX - 1, currentNode.gridY];
                neighbours[2] = grid[currentNode.gridX, currentNode.gridY + 1];
                neighbours[3] = grid[currentNode.gridX, currentNode.gridY - 1];

                foreach (Node neighbour in neighbours)
                {
                    if (!neighbour.walkable || closedSet.Contains(neighbour))
                    {
                        continue;
                    }

                    int newMovementCostToNeighbour = currentNode.gCost + GetDistance(currentNode, neighbour);

                    if (newMovementCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour))
                    {
                        neighbour.gCost = newMovementCostToNeighbour;
                        neighbour.hCost = GetDistance(neighbour, targetNode);
                        neighbour.parent = currentNode;

                        if (!openSet.Contains(neighbour))
                        {
                            openSet.Add(neighbour);
                        }
                    }
                }
            }
        }

        public void RetracePath(Node startNode, Node endNode)
        {
            List<Node> path = new List<Node>();
            Node currentNode = endNode;

            while (currentNode != startNode)
            {
                path.Add(currentNode);
                currentNode = currentNode.parent;
            }

            path.Reverse();

            this.path = path;
        }

        public int GetDistance(Node nodeA, Node nodeB)
        {
            int dstX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
            int dstY = Mathf.Abs(nodeA.gridY - nodeB.gridY);

            if (dstX > dstY)
            {
                return 14 * dstY + 10 * (dstX - dstY);
            }
            return 14 * dstX + 10 * (dstY - dstX);
        }


        void OnDrawGizmos()
        {

            if (grid != null)
            {
                foreach (Node n in grid)
                {
                    Gizmos.color = (n.walkable) ? Color.white : Color.red;
                    if (path != null)
                    {
                        if (path.Contains(n))
                        {
                            Gizmos.color = Color.black;
                        }
                    }
                    Gizmos.DrawCube(new Vector3(n.gridX, n.gridY, 0), Vector3.one * (1 - .1f));
                }
            }
        }
    }

}
