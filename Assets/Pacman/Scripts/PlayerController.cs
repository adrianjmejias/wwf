﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {


    public bool isInvinsible;
    public int invinsibleTime;
    public Sprite invinsibleSprite;
    public Sprite regularSprite;
    private Animator animator;

	// Use this for initialization
	void Start () {
        invinsibleTime = 0;
        animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public IEnumerator GetInvinsible()
    {
        invinsibleTime = 10;
        //GetComponent<Renderer>().material = invinsibleMat;
        isInvinsible = true;

        while (invinsibleTime > 0)
        {
            invinsibleTime--;
            yield return new WaitForSeconds(1f);
        }

        isInvinsible = false;
        //GetComponent<Renderer>().material = regularMat;
    }

    public void ChangeAnimation(Vector3 moveDir)
    {
        if(moveDir.x == -1)
        {
            animator.SetInteger("moveDirX", -1);
            animator.SetInteger("moveDirY", 0);
        }
        else if(moveDir.x == 1)
        {
            animator.SetInteger("moveDirX", 1);
            animator.SetInteger("moveDirY", 0);
        }
        else if (moveDir.y == 1)
        {
            animator.SetInteger("moveDirX", 0);
            animator.SetInteger("moveDirY", 1);
        }
        else if (moveDir.y == -1)
        {
            animator.SetInteger("moveDirX", 0);
            animator.SetInteger("moveDirY", -1);
        }

    }
}
