﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoletaGaming.Pacman
{
    public class MapGenerator : MonoBehaviour
    {

        private const int NUMBER_OF_LIFE = 1;
        private const int FOOD = 2;
        private const int WALL = 1;
        private const int FREE = 0;
        private const int PACMAN = -1;
        private const int POWER = 3;
        private const int BLINKY = 4;
        private const int INKY = 5;
        private const int PINKY = 6;
        private const int CLYDE = 7;

        private Color pacmanColor = new Color(1, 1, 0); //yellow
        private Color blinkyColor = new Color(0, 0, 1); //blue
        private Color inkyColor = new Color(1, 0.498f, 0); //orange
        private Color pinkyColor = new Color(1, 0, 1); //pink
        private Color clydeColor = new Color(1, 0, 0); //red
        private Color wallColor = new Color(0,0,0); //black
        private Color freeColor = new Color( 0.498f, 0.498f, 0.498f); //gray
        private Color foodColor = new Color(1, 1, 1); //white
        private Color powerColor = new Color(0, 1, 1); //turquese
        private Color32 inkyColor32 = new Color32(255, 127, 0 , 255);

        public int numberOfFood = 0;

        public List<Texture2D> maps;

        public int[,] map;
        public GameObject[,] objectMap;

        public GameObject wallPrefab;
        public GameObject floorPrefab;
        public GameObject foodPrefab;
        public GameObject powerPrefab;
        public GameObject pacmanPrefab;
        public GameObject ghostPrefab;


        public Vector3 blinkyStartPos;
        public Vector3 pacmanStartPos;
        public Vector3 inkyStartPos;
        public Vector3 clydeStartPos;
        public Vector3 pinkyStartPos;

        public GameObject blinky;
        public GameObject inky;
        public GameObject pinky;
        public GameObject clyde;
        public GameObject pacman;

        public Transform mapHolder;

        public void InstantiateGhosts()
        {

            blinky = Instantiate(ghostPrefab, mapHolder);
            blinky.transform.position = new Vector3(blinkyStartPos.x, blinkyStartPos.y, 0);
            objectMap[(int)blinkyStartPos.x, (int)blinkyStartPos.y] = blinky;
            map[(int)blinkyStartPos.x, (int)blinkyStartPos.y] = BLINKY;


            inky = Instantiate(ghostPrefab, mapHolder);
            inky.transform.position = new Vector3(inkyStartPos.x, inkyStartPos.y, 0);
            objectMap[(int)inkyStartPos.x, (int)inkyStartPos.y] = inky;
            map[(int)inkyStartPos.x, (int)inkyStartPos.y] = INKY;

            pinky = Instantiate(ghostPrefab, mapHolder);
            pinky.transform.position = new Vector3(pinkyStartPos.x, pinkyStartPos.y, 0);
            objectMap[(int)pinkyStartPos.x, (int)pinkyStartPos.y] = pinky;
            map[(int)pinkyStartPos.x, (int)pinkyStartPos.y] = PINKY;



            clyde = Instantiate(ghostPrefab, mapHolder);
            clyde.transform.position = new Vector3(clydeStartPos.x, clydeStartPos.y, 0);
            objectMap[(int)clydeStartPos.x, (int)clydeStartPos.y] = clyde;
            map[(int)clydeStartPos.x, (int)clydeStartPos.y] = CLYDE;

        }

        public void InstantiatePlayer()
        {
            pacman = Instantiate(pacmanPrefab, mapHolder);
            pacman.transform.position = new Vector3(pacmanStartPos.x, pacmanStartPos.y, 0);
            objectMap[(int)pacmanStartPos.x, (int)pacmanStartPos.y] = pacman;
            map[(int)blinkyStartPos.x, (int)blinkyStartPos.y] = PACMAN;
        }

        public void CleanGhosts()
        {
            for (int x = 0; x < map.GetLength(0); x++)
            {
                for (int y = 0; y < map.GetLength(1); y++)
                {
                    int item = map[x, y];
                    if(item != FOOD || item != PACMAN || item != POWER || item != FREE)
                    {
                        map[x, y] = FOOD;
                        GameObject food = Instantiate(foodPrefab, mapHolder);
                        food.transform.position = new Vector3(x, y, 1);
                        objectMap[x, y] = food;
                    }
                }
            }

            map[(int)blinkyStartPos.x, (int)blinkyStartPos.y] = BLINKY;
            map[(int)inkyStartPos.x, (int)inkyStartPos.y] = INKY;
            map[(int)pinkyStartPos.x, (int)pinkyStartPos.y] = PINKY;
            map[(int)clydeStartPos.x, (int)clydeStartPos.y] = CLYDE;
            map[(int)pacmanStartPos.x, (int)pacmanStartPos.y] = PACMAN;
        }

        public void printMap()
        {
            for(int i = 0; i < map.GetLength(0); i++)
            {
                string str = "";
                for(int j = 0; j < map.GetLength(1); j++)
                {
                    str += map[i, j] + " ";
                }

                Debug.Log(str);
            }
        }


        public void GenerateMap(int indexMap)
        {
            mapHolder.Clear();
            Texture2D mapTexture = maps[indexMap];

            map = new int[mapTexture.width, mapTexture.height];

            Debug.Log(map.GetLength(1));
            Debug.Log(map.GetLength(0));

            objectMap = new GameObject[map.GetLength(0), map.GetLength(1)];


            for (int x = 0; x < map.GetLength(0); x++)
            {
                for (int y = 0; y < map.GetLength(1); y++)
                {
                    
                    GameObject floor = Instantiate(floorPrefab, mapHolder);
                    floor.transform.position = new Vector3(x, y, 2);

                    Debug.Log(mapTexture.GetPixel(x, y));


                    if (mapTexture.GetPixel(x,y) == pacmanColor)
                    {
                        map[x, y] = PACMAN;
                        pacman = Instantiate(pacmanPrefab, mapHolder);
                        pacmanStartPos = new Vector3(x, y, 0);
                        pacman.transform.position = new Vector3(x, y, 0);
                        objectMap[x, y] = pacman;
                    }
                    else if (mapTexture.GetPixel(x, y) == wallColor)
                    {
                        map[x, y] = WALL;
                        GameObject wall = Instantiate(wallPrefab, mapHolder);
                        wall.transform.position = new Vector3(x, y, 1);
                        objectMap[x, y] = wall;
                    }
                    else if (mapTexture.GetPixel(x, y) == foodColor)
                    {
                        map[x, y] = FOOD;
                        GameObject food = Instantiate(foodPrefab, mapHolder);
                        food.transform.position = new Vector3(x, y, 1);
                        objectMap[x, y] = food;

                        numberOfFood++;
                    }
                    else if (mapTexture.GetPixel(x, y) == powerColor)
                    {
                        map[x, y] = POWER;
                        GameObject power = Instantiate(powerPrefab, mapHolder);
                        power.transform.position = new Vector3(x, y, 1);
                        objectMap[x, y] = power;

                        numberOfFood++;
                    }
                    else if (mapTexture.GetPixel(x, y) == blinkyColor)
                    {
                        map[x, y] = BLINKY;
                        blinky = Instantiate(ghostPrefab, mapHolder);
                        blinkyStartPos = new Vector3(x, y, 0);
                        blinky.transform.position = new Vector3(x, y, 0);
                        objectMap[x, y] = blinky;
                    }
                    else if (mapTexture.GetPixel(x, y) == inkyColor32)
                    {
                        map[x, y] = INKY;
                        inky = Instantiate(ghostPrefab, mapHolder);
                        inkyStartPos = new Vector3(x, y, 0);
                        inky.transform.position = new Vector3(x, y, 0);
                        objectMap[x, y] = inky;
                    }
                    else if (mapTexture.GetPixel(x, y) == pinkyColor)
                    {
                        map[x, y] = PINKY;
                        pinky = Instantiate(ghostPrefab, mapHolder);
                        pinkyStartPos = new Vector3(x, y, 0);
                        pinky.transform.position = new Vector3(x, y, 0);
                        objectMap[x, y] = pinky;
                    }
                    else if (mapTexture.GetPixel(x, y) == clydeColor)
                    {
                        map[x, y] = CLYDE;
                        clyde = Instantiate(ghostPrefab, mapHolder);
                        clydeStartPos = new Vector3(x, y, 0);
                        clyde.transform.position = new Vector3(x, y, 0);
                        objectMap[x, y] = clyde;
                    }
                }
            }

            //Debug.Log("Ya termine");
            printMap();
        }
    }

}
