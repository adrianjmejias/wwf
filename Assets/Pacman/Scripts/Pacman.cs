﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BoletaGaming.AudioGraph;
using UnityEngine.UI;
using BoletaGaming.WWF.Minigame;
using BoletaGaming.WWF;

namespace BoletaGaming.Pacman
{
    public enum GameStates
    {
        paused,
        playing,
        playerSet,
        playerDead
    }

    public class Pacman : Minigame
    {


        //Audios
        public AudioClipReference eatSound;
        public AudioClipReference dieSound;
        public AudioClipReference win;
        public AudioClipReference lose;

        public Sprite pacmanSide;
        public Sprite pacmanUp;


        public static Pacman instance;

        [SerializeField] private AudioNode gameSound;

        [Range(0,1)]
        public float interpolationValue1;

        [Range(0,2)]
        public float interpolationValue2;


        private const int NUMBER_OF_LIFE = 1;
        private const int FOOD = 2;
        private const int WALL = 1;
        private const int FREE = 0;
        private const int PACMAN = -1;
        private const int POWER = 3;
        private const int BLINKY = 4;
        private const int INKY = 5;
        private const int PINKY = 6;
        private const int CLYDE = 7;


        public int currentLvl;

        int numberOfPower = 4;
        int numberOfFood;

        //portalPos
        Vector3 portalLeft1;
        Vector3 portalLeft2;
        Vector3 portalRight1;
        Vector3 portalRight2;



        private int score;
        private int life = NUMBER_OF_LIFE;

        public Coroutine playerMoveC = null;
        public GameStates gameState;

        public GameObject player;
        public GameObject blinky;
        public GameObject inky;
        public GameObject pinky;
        public GameObject clyde;

        public Transform mapHolder;

        public AudioNode audioSource;

        public int[,] map;
        public GameObject[,] objectMap;
        private Vector3 moveDir;

        public void StartGame(bool firstTime, int indexLvl)
        {

            moveDir = new Vector3(0, 0, 0);
            gameState = GameStates.paused;

            MapGenerator mapGenerator = GetComponent<MapGenerator>();

            if (firstTime)
            {

                numberOfPower = 4;
                score = 0;
                life = NUMBER_OF_LIFE;
                mapGenerator.GenerateMap(currentLvl);
                map = mapGenerator.map;


                portalLeft1 = new Vector3(0, 14, 0);
                portalLeft2 = new Vector3(0, 9, 0);
                portalRight1 = new Vector3(map.GetLength(0), 14, 0);
                portalRight2 = new Vector3(map.GetLength(0), 9, 0);

                numberOfFood = mapGenerator.numberOfFood;


                objectMap = mapGenerator.objectMap;
                clyde = mapGenerator.clyde;
                blinky = mapGenerator.blinky;
                pinky = mapGenerator.pinky;
                inky = mapGenerator.inky;
                player = mapGenerator.pacman;

                gameState = GameStates.playing;
                playerMoveC = StartCoroutine(MovePlayer());
                SetUpGhosts();

            }
            else
            {


                clyde.GetComponent<Ghost>().Destroy();
                pinky.GetComponent<Ghost>().Destroy();
                inky.GetComponent<Ghost>().Destroy();
                blinky.GetComponent<Ghost>().Destroy();

                int playerX = (int)player.transform.position.x;
                int playerY = (int)player.transform.position.y;
                map[playerX, playerY] = 0;
                objectMap[playerX, playerY] = null;

                mapGenerator.map = map;
                mapGenerator.objectMap = objectMap;


                Destroy(clyde);
                Destroy(pinky);
                Destroy(inky);
                Destroy(blinky);
                Destroy(player);
                StopCoroutine(playerMoveC);

                //mapGenerator.CleanGhosts();
                mapGenerator.InstantiateGhosts();
                mapGenerator.InstantiatePlayer();

                map = mapGenerator.map;
                objectMap = mapGenerator.objectMap;
                clyde = mapGenerator.clyde;
                blinky = mapGenerator.blinky;
                pinky = mapGenerator.pinky;
                inky = mapGenerator.inky;
                player = mapGenerator.pacman;


                gameState = GameStates.playing;
                playerMoveC = StartCoroutine(MovePlayer());
                SetUpGhosts();
            }


        }



        public void FinishGame()
        {
            Destroy(player);

            life--;
            if (life > 0)
            {
                StartGame(false, currentLvl);
            }
            else
            {
                MG_End(false);
            }

        }

        public void WinGame()
        {
            MG_End(true);
        }


        public void SetUpGhosts()
        {
            blinky.GetComponent<Ghost>().SetUpGhost(BLINKY);
            inky.GetComponent<Ghost>().SetUpGhost(INKY);
            pinky.GetComponent<Ghost>().SetUpGhost(PINKY);
            clyde.GetComponent<Ghost>().SetUpGhost(CLYDE);
        }

        private void Awake()
        {

            
            if (instance == null)//Check if instance already exists
            {
                //if not, set instance to this
                instance = this;
            }
            else if (instance != this)//If instance already exists and it's not this:
            {
                //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a Pacman.
                Destroy(gameObject);
            }


        }

        // Update is called once per frame
        void Update()
        {
            GetComponent<UIManager>().UpdateScore(score);
            GetComponent<UIManager>().UpdateLife(life);

            if (gameState == GameStates.playing)
            {
                moveDir = GetInput();
            }

        }

        IEnumerator MovePlayer()
        {

            while (gameState == GameStates.playing && player != null)
            {

                //SpriteRenderer sp = player.GetComponent<SpriteRenderer>();
                player.GetComponent<PlayerController>().ChangeAnimation(moveDir);

                int nextX = (int)player.transform.position.x + (int)moveDir.x;
                int nextY = (int)player.transform.position.y + (int)moveDir.y;

                int currentX = (int)player.transform.position.x;
                int currentY = (int)player.transform.position.y;

                /*

                if (map[nextX, nextY] == PACMAN)
                {
                    yield return null;
                    continue;
                }

                */


                if ((currentX == 0 && currentY == 9) || (currentX == 0 && currentY == 14))
                {
                    if (moveDir.x == -1)
                    {
                        map[currentX, currentY] = 0;
                        objectMap[currentX, currentY] = null;

                        if (currentY == 9)
                        {
                            nextX = map.GetLength(0) - 1;
                            nextY = 9;

                            player.transform.position = new Vector3(nextX, nextY, 0);
                            Destroy(objectMap[nextX, nextY]);
                            objectMap[nextX, nextY] = player;
                            map[nextX, nextY] = -1;
                        }
                        else
                        {
                            nextX = map.GetLength(0) - 1;
                            nextY = 14;

                            player.transform.position = new Vector3(nextX, nextY, 0);
                            Destroy(objectMap[nextX, nextY]);
                            objectMap[nextX, nextY] = player;
                            map[nextX, nextY] = -1;
                        }

                        continue;
                    }
                }
                else if ((currentX == map.GetLength(0) - 1 && currentY == 9) || (currentX == map.GetLength(0) - 1 && currentY == 14))
                {
                    if (moveDir.x == 1)
                    {
                        map[currentX, currentY] = 0;
                        objectMap[currentX, currentY] = null;

                        if (currentY == 9)
                        {
                            nextX = 0;
                            nextY = 9;

                            player.transform.position = new Vector3(nextX, nextY, 0);
                            Destroy(objectMap[nextX, nextY]);
                            objectMap[nextX, nextY] = player;
                            map[nextX, nextY] = -1;
                        }
                        else
                        {
                            nextX = 0;
                            nextY = 14;

                            player.transform.position = new Vector3(nextX, nextY, 0);
                            Destroy(objectMap[nextX, nextY]);
                            objectMap[nextX, nextY] = player;
                            map[nextX, nextY] = -1;
                        }

                        continue;
                    }
                }


                if (map[nextX, nextY] != WALL)
                {
                    if (map[nextX, nextY] == FOOD)
                    {
                        score += 10;
                        numberOfFood--;
                        audioSource.PlaySFX(eatSound);

                        Debug.Log("Comidas restantes: " + numberOfFood);
                    }
                    else if (map[nextX, nextY] == POWER)
                    {
                        numberOfPower--;
                        numberOfFood--;
                        score += 15;
                        StartCoroutine(player.GetComponent<PlayerController>().GetInvinsible());

                        Debug.Log("Comidas restantes: " + numberOfFood);
                        audioSource.PlaySFX(eatSound);

                    }
                    else if (map[nextX, nextY] == BLINKY || map[nextX, nextY] == INKY || map[nextX, nextY] == PINKY || map[nextX, nextY] == CLYDE)
                    {
                        /* interpolate movement*/
                        yield return StartCoroutine(InterpolateMovement(new Vector3(currentX, currentY, 0), new Vector3(nextX, nextY, 0), player.gameObject));
                        /* finish interpolation*/

                        map[currentX, currentY] = 0;
                        objectMap[currentX, currentY] = null;


                        FinishGame();
                        break;
                    }

                    if (numberOfFood <= 0)
                    {
                        WinGame();
                    }

                    //portals
                   
                    map[currentX, currentY] = 0;
                    objectMap[currentX, currentY] = null;

                    player.transform.position = new Vector3(nextX, nextY, 0);
                    Destroy(objectMap[nextX, nextY]);
                    objectMap[nextX, nextY] = player;
                    map[nextX, nextY] = -1;

                    /* interpolate movement*/
                    yield return StartCoroutine(InterpolateMovement(new Vector3(currentX, currentY, 0), new Vector3(nextX, nextY, 0), player.gameObject));
                    /* finish interpolation*/

                }

                yield return null;
            }

        }

        public Vector3 GetInput()
        {

            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                return new Vector3(0, 1, 0);
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                return new Vector3(0, -1, 0);
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                return new Vector3(-1, 0, 0);
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                return new Vector3(1, 0, 0);
            }

            return moveDir;
        }


        public IEnumerator InterpolateMovement(Vector3 start, Vector3 end, GameObject obj)
        {
            float t = 0;
            float complete = 1;

            //Debug.Log("interpolando");
            while (t < complete)
            {
                float newPosX = Mathf.Lerp(start.x, end.x, t);
                float newPosY = Mathf.Lerp(start.y, end.y, t);
                obj.transform.position = new Vector3(newPosX, newPosY, 0);
                yield return new WaitForSeconds(interpolationValue2);

                t += interpolationValue1;
            }
            if (obj)
            {
                Debug.LogWarning("object has been destroyed");
                obj.transform.position = new Vector3(end.x, end.y, 0);

            }

        }

        public IEnumerator KillPacman(GameObject ghost, int nextX_ghost, int nextY_ghost)
        {
            Vector3 start1 = new Vector3((int)ghost.transform.position.x, (int)ghost.transform.position.y, 0);
            Vector3 start2 = new Vector3((int)player.transform.position.x, (int)player.transform.position.y, 0);
            Vector3 end = new Vector3(nextX_ghost, nextY_ghost, 0);

            yield return StartCoroutine(InterpolateMovement(start1, end, ghost));
            yield return StartCoroutine(InterpolateMovement(start2, end, player));

        }

        public override void MG_Reset()
        {
            throw new System.NotImplementedException();
        }

        public override void MG_Quit()
        {
            throw new System.NotImplementedException();
        }

        public override void MG_Pause()
        {
            throw new System.NotImplementedException();
        }

        public override void MG_Start(int _id)
        {
            base.MG_Start(_id);
           

            Camera.main.orthographicSize = 12;
            currentLvl = _id;

            StartGame(true, currentLvl);

        }

        public override void MG_End(bool _win)
        {
            //mapCanvas.renderMode = RenderMode.ScreenSpaceOverlay;
            if (_win)
            {
                audioSource.PlaySFX(win);

            }
            else
            {
                audioSource.PlaySFX(lose);
                Debug.Log("perdiste!");
            }
            mapHolder.Clear();
            GameManager.Instance.EndMinigame(_win);
        }

        public override int GetNumLevels()
        {
            return 3 - numMixLevels;
        }


}

    [System.Serializable]
    public class SFX
    {

    }


}
