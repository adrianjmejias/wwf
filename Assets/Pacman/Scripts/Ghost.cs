﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoletaGaming.Pacman
{
    public class Ghost : MonoBehaviour
    {
        private const int NUMBER_OF_LIFE = 1;
        private const int FOOD = 2;
        private const int WALL = 1;
        private const int FREE = 0;
        private const int PACMAN = -1;
        private const int POWER = 3;
        private const int BLINKY = 4;
        private const int INKY = 5;
        private const int PINKY = 6;
        private const int CLYDE = 7;


        Coroutine moveGhostC;

        public Transform mapHolder;

        private int ghostNumber;
        public states currentState;
        public List<Node> path;
        public GameObject[] objectPrefab;
        public GameObject player;
        public PathFinding pathFinding;
        public GameObject[,] objectMap;
        public int currentItemCell;
        public int[,] map;

        public int timeScatter = 10;
        public float timeChase = 5;
        public float scatterTime;
        public float currentTime;

        public Vector3 facing;

        public enum states
        {
            scatter,
            chase,
            frightened,
            dead,
            waitingToSetUp,
            playerDead
        }

        public void SetUpGhost(int number)
        {

            ghostNumber = number;
            currentTime = 0;
            facing = new Vector3(0, -1, 0);
            currentItemCell = 0;
            currentState = states.chase;
            mapHolder = Pacman.instance.mapHolder;
            player = Pacman.instance.player;
            map = Pacman.instance.map;

            SetUpPathFinding();


            moveGhostC = StartCoroutine(StartGhost());
        }

        // Update is called once per frame
        void Update()
        {

        }

        public IEnumerator KillPlayer()
        {
            yield return new WaitForSeconds(1f);
        }

        public IEnumerator StartGhost()
        {


            while (currentState != states.dead || player != null)
            {


                int posX;
                int posY;
                int nextX;
                int nextY;

                int mapWidth = map.GetLength(0) - 1;
                int mapHeight = map.GetLength(1) - 1;

                switch (currentState)
                {


                    case states.scatter:

                        /*finding a random position in map*/
                        if (currentTime > timeScatter)
                        {
                            currentTime = 0;
                            currentState = states.chase;
                            continue;
                        }

                        currentTime++;

                        int randomX;
                        int randomY;

                        randomX = Random.Range(1, mapWidth);
                        randomY = Random.Range(1, mapHeight);


                        if (map[randomX, randomY] == WALL) continue;
                        //Debug.Log("Soy el fantasma " + ghostNumber + " y me voy a mover a: " + randomX + ", " + randomY);

                        pathFinding.FindPath(transform.position, new Vector3(randomX, randomY, 0));

                        path = pathFinding.path;

                        if(path.Count > 0)
                        {
                            nextX = path[0].gridX;
                            nextY = path[0].gridY;
                        }
                        else
                        {
                            continue;
                        }
                        

                        //Debug.Log("Soy el fantasma " + ghostNumber + " y me voy a mover a: " + nextX + ", " + nextY);

                        posX = (int)transform.position.x;
                        posY = (int)transform.position.y;

                        if ((Mathf.Abs(nextX - posX) + Mathf.Abs(nextY - posY)) >= 2) continue;
                        
                        facing = new Vector3(nextX - posX, 0, nextY - posY);

                        if (Pacman.instance.map[nextX, nextY] == FOOD ||
                            Pacman.instance.map[nextX, nextY] == POWER ||
                            Pacman.instance.map[nextX, nextY] == FREE)
                        {

                            yield return StartCoroutine(Move(posX, posY, nextX, nextY));

                            SpriteRenderer sp = GetComponent<SpriteRenderer>();
                            if (facing.x == -1) sp.flipX = false;
                            else sp.flipX = true;
                        }
                        else if (Pacman.instance.map[nextX, nextY] == PACMAN)
                        {
                            yield return StartCoroutine(Pacman.instance.KillPacman(gameObject, nextX, nextY));
                            Pacman.instance.FinishGame();
                        }

                        path.RemoveAt(0);


                        break;

                        

                    case states.frightened:
                        break;

                    case states.chase:

                        if (currentTime > timeChase)
                        {
                            currentTime = 0;
                            currentState = states.scatter;
                            continue;
                        }

                        currentTime ++;

                        pathFinding.FindPath(transform.position, player.transform.position);

                        path = pathFinding.path;

                        if (path.Count > 0)
                        {
                            nextX = path[0].gridX;
                            nextY = path[0].gridY;
                        }
                        else
                        {
                            continue;
                        }

                        posX = (int)transform.position.x;
                        posY = (int)transform.position.y;

                        facing = new Vector3(nextX - posX, 0, nextY - posY);

                        if (Pacman.instance.map[nextX, nextY] == FOOD ||
                            Pacman.instance.map[nextX, nextY] == POWER ||
                            Pacman.instance.map[nextX, nextY] == FREE)
                        {

                            yield return StartCoroutine(Move(posX, posY, nextX, nextY));

                            SpriteRenderer sp = GetComponent<SpriteRenderer>();
                            if (facing.x == -1) sp.flipX = false;
                            else sp.flipX = true;


                        }
                        else if (Pacman.instance.map[nextX, nextY] == PACMAN) //encuentra al pacman
                        {
                            yield return StartCoroutine(Pacman.instance.KillPacman(gameObject, nextX, nextY));

                            Pacman.instance.FinishGame();
                        }

                        break;

                }


                yield return null;
            }

        }



        public void SetUpPathFinding()
        {
            pathFinding = GetComponent<PathFinding>();
            pathFinding.seeker = transform;
            pathFinding.target = player.transform;
            pathFinding.CreateGridFromMap(map);
        }

        public void StopPathFinding()
        {
            pathFinding.seeker = null;
            pathFinding.target = null;

        }

        public void Destroy()
        {
            StopCoroutine(moveGhostC);
            Debug.Log("current item: " + currentItemCell);
            int posX = (int)transform.position.x;
            int posY = (int)transform.position.y;
            UpdateCurrentPos(posX, posY);

        }

        public IEnumerator Move(int startPosX, int startPosY, int endPosX, int endPosY)
        {

            /*actualizo la posicion actual*/
            UpdateCurrentPos(startPosX, startPosY);
            UpdateNextPos(endPosX, endPosY);
            yield return StartCoroutine(Pacman.instance.InterpolateMovement(new Vector3(startPosX, startPosY, 0), new Vector3(endPosX, endPosY, 0), gameObject));
        }

        public void UpdateCurrentPos(int startPosX, int startPosY)
        {
            if (currentItemCell == 2)
            {
                GameObject food = Instantiate(objectPrefab[0], mapHolder);
                food.transform.position = new Vector3(startPosX, startPosY, 1);
                Pacman.instance.objectMap[startPosX, startPosY] = food;
                Pacman.instance.map[startPosX, startPosY] = currentItemCell;

            }
            else if (currentItemCell == 3)
            {
                GameObject power = Instantiate(objectPrefab[1], mapHolder);
                power.transform.position = new Vector3(startPosX, startPosY, 1);
                Pacman.instance.objectMap[startPosX, startPosY] = power;
                Pacman.instance.map[startPosX, startPosY] = currentItemCell;
            }
            else
            {
                Pacman.instance.objectMap[startPosX, startPosY] = null;
                Pacman.instance.map[startPosX, startPosY] = currentItemCell;
            }
        }

        public void UpdateNextPos(int endPosX, int endPosY)
        {
            /*actualizo la posicion siguiente*/
            currentItemCell = Pacman.instance.map[endPosX, endPosY];
            Destroy(Pacman.instance.objectMap[endPosX, endPosY]);
            Pacman.instance.objectMap[endPosX, endPosY] = gameObject;
            Pacman.instance.map[endPosX, endPosY] = ghostNumber;
            transform.position = new Vector3(endPosX, endPosY, 0);
        }
    }



}
