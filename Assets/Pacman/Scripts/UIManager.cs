﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BoletaGaming.Pacman
{
    public class UIManager : MonoBehaviour
    {
        public Text scoreText;
        public Text lifeText;
        public static UIManager instance;

        // Use this for initialization
        void Start()
        {



        }

        private void Awake()
        {
            //Check if instance already exists
            if (instance == null)

                //if not, set instance to this
                instance = this;

            //If instance already exists and it's not this:
            else if (instance != this)

                //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a Pacman.
                Destroy(gameObject);


        }

        public void UpdateScore(int score)
        {
            scoreText.text = "Score: " + score.ToString();
        }

        public void UpdateLife(int life)
        {
            // lifeText.text = "Life: " + life.ToString();
        }
    }

}
