﻿using BoletaGaming.WWF.Minigame;
using BoletaGaming.WWF;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace BoletaGaming.RotatePuzzle
{
    [System.Serializable]
    public class Puzzle
    {
        public GameObject parentImage;
        public GameObject[] images;
    }


    public class RotatePuzzle : Minigame
    {

        public Puzzle[] levels;
        public int currentLevel;

        // Update is called once per frame
        void Update()
        {
            if (checkForWin())
                MG_End(true);
        }

        public bool checkForWin()
        {
            for (int i = 0; i < levels[currentLevel].images.Length; i++)
            {
                if (levels[currentLevel].images[i].transform.localRotation.z != 0) return false;
            }

            return true;
        }

        public override void MG_End(bool _win)
        {
            if (_win) Debug.Log("GANASTE EL MIO!!");
        }

        public override void MG_Pause()
        {
            throw new System.NotImplementedException();
        }

        public override void MG_Quit()
        {
            throw new System.NotImplementedException();
        }

        public override void MG_Reset()
        {
            throw new System.NotImplementedException();
        }

        public override void MG_Start(int _id)
        {
            currentLevel = _id;
            Instantiate(levels[_id].parentImage,transform);

            Transform imageHolderT = transform.GetChild(0);

            for (int i = 0; i < imageHolderT.childCount; i++)
            {
                levels[_id].images[i] = imageHolderT.GetChild(i).gameObject;

            }

        }

        public override int GetNumLevels()
        {
            throw new System.NotImplementedException();
        }
    }

}
