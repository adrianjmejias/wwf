﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BoletaGaming.WWF.Minigame;
using BoletaGaming.WWF;
using BoletaGaming.AudioGraph;
using BoletaGaming.HolderCombo;
using System.Linq;
using NaughtyAttributes;
namespace BoletaGaming.TriviaQuestions
{

    public class TriviaQuestions : Minigame, ShuffleableMinigame
    {
        const int SOLUTION = 0;

        [BoxGroup("Database")] [SerializeField] private TriviaQuestionLevel[] levels;

        [BoxGroup("UI")] [SerializeField] private Text textQuestion;
        [BoxGroup("UI")] [SerializeField] private HolderCombo.HolderCombo answerCombo;
        

        [BoxGroup("Sound")] public AudioNode gameSound;
        [BoxGroup("Sound")] public SFX sfx;

        [BoxGroup("State")] [SerializeField] private List<TriviaQuestionsOpt> opts = new List<TriviaQuestionsOpt>();
        [BoxGroup("State")] [SerializeField] private TriviaQuestionLevel actLevel;

        public override void MG_Start(int _id)
        {
            base.MG_Start(_id);
            actLevel = levels[_id];
            SetUI(actLevel);
        }

        public override void MG_End(bool _win)
        {
            base.MG_End(_win);
            if (_win)
            {
                gameSound.PlaySFX(sfx.Win);
            }
            else
            {
                gameSound.PlaySFX(sfx.Lose);
            }

        }

        public override void MG_Pause()
        {

        }

        public override void MG_Quit()
        {
            throw new System.NotImplementedException();
        }

        public override void MG_Reset()
        {
            throw new System.NotImplementedException();
        }

        public void SetUI(TriviaQuestionLevel value)
        {
            int _iLength, _vLength; // cache, evitar llamadas a count y bla bla bla
            _iLength = opts.Count;
            _vLength = value.options.Length;

            // Si he instanciado menos opciones de las que necesito, instanciame más
            while (_iLength < _vLength)
            {
                TriviaQuestionsOpt opt = answerCombo.Get().GetComponent<TriviaQuestionsOpt>();
                opts.Add(opt);
                _iLength++;
            }

            string[] _opt = (string[])value.options.Clone();

            _opt.Shuffle();

            // LLenando preguntas
            for (int ii = 0; ii < _vLength; ii++)
            {
                opts[ii].val = _opt[ii];
                opts[ii].button.onClick.RemoveAllListeners();
                int cII = ii; // clojure
                opts[ii].button.onClick.AddListener(() =>
                {
                    MG_End(opts[cII].val == value.options[SOLUTION]);
                });
                opts[ii].gameObject.SetActive(true);
            }

            // si tengo más de las que necesito instanciadas, apagar
            for (int ii = _vLength; ii < _iLength; ii++)
            {
                opts[ii].gameObject.SetActive(false);
            }

            textQuestion.text = value.question;
        }

        public override int GetNumLevels()
        {
            return levels.Length - numMixLevels;
        }

        void ShuffleableMinigame.Shuffle()
        {
            levels.Shuffle();
        }
    }


    [System.Serializable]
    public class SFX
    {
        public AudioClipReference Win;
        public AudioClipReference Lose;
    }
}
