﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BoletaGaming.TriviaQuestions
{
    [RequireComponent(typeof(Button))]
    [RequireComponent(typeof(Image))]
    public class TriviaQuestionsOpt : MonoBehaviour
    {
        [SerializeField] private Text Pval;
        [SerializeField] public Button button;
        public Image background;
        public string val
        {
            set
            {
                Pval.text = value;
            }
            get
            {
                return Pval.text;
            }
        }
    }
}