﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

namespace BoletaGaming.TriviaQuestions
{
    [CreateAssetMenu(fileName = "TQuestion Level ", menuName = "Create TQuestion Level")]
    public class TriviaQuestionLevel : ScriptableObject
    {

        [InfoBox("El primer elemento del array de opciones es considerada la solución")]
        public string question;
        [ReorderableList]
        public string[] options;
    }
}
