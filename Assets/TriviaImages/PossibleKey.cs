﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace BoletaGaming.TriviaImages
{
    public class PossibleKey : MonoBehaviour
    {
        public Button button;
        [SerializeField] private Text PVal;
        public string val
        {
            get
            {
                return PVal.text;
            }
            set
            {
                PVal.text = value;
            }
        }
        public void Click(TriviaImages trivia)
        {
            trivia.Click(this);
        }
    }
}
