﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace BoletaGaming.TriviaImages
{
    public class SolutionKey : MonoBehaviour
    {
        public Button button;
        [SerializeField] private Text PVal;
        public string val
        {
            set
            {
                PVal.text = value;
            }
            get
            {
                return PVal.text;
            }
        }
        [System.NonSerialized] public PossibleKey filledMe;
        public void Click(TriviaImages trivia)
        {
            trivia.Click(this);
        }
    }
}
