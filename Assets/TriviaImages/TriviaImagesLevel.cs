﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using BoletaGaming.WWF.Minigame;

namespace BoletaGaming.TriviaImages
{
    [CreateAssetMenu(fileName = "TImages Level ", menuName = "Create TImages Level")]
    public class TriviaImagesLevel : ScriptableObject
    {
        [InfoBox("El nombre del sprite es tomado como la respuesta a la pregunta")]
        [Required]
         public Sprite question;
    }
}
