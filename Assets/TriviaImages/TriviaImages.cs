﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BoletaGaming.WWF.Minigame;
using BoletaGaming.WWF;
using BoletaGaming.AudioGraph;
using System.Linq;
using NaughtyAttributes;

namespace BoletaGaming.TriviaImages
{
    public class TriviaImages : Minigame, ShuffleableMinigame
    {
        public const string EMPTY_LETTER = "";
        public const int POSSIBLE_MAX = 16;
        public const string simbols = "abcdefghijklmnñopqrstuvwxyz";
        public const int MAX_SELECTION = 2;

        [BoxGroup("Database")] [SerializeField] private TriviaImagesLevel[] levels;

        [BoxGroup("UI")] [SerializeField] Image image;
        [BoxGroup("UI")] [SerializeField] public HolderCombo.HolderCombo solutionInstantiate;
        [BoxGroup("UI")] [SerializeField] public HolderCombo.HolderCombo possibleInstantiate;

        [BoxGroup("Sound")] public AudioNode gameSound;
        [BoxGroup("Sound")] public SFX sfxs;

        [BoxGroup("State")] private List<SolutionKey> solutionKeys = new List<SolutionKey>();
        [BoxGroup("State")] private List<PossibleKey> possibleKeys = new List<PossibleKey>();
        [BoxGroup("State")] public TriviaImagesLevel actLevel;
        [BoxGroup("State")] public int index = 0;

        
        public override void MG_End(bool _win)
        {
            if (_win)
            {
                gameSound.PlaySFX(sfxs.Win);

            }
            else
            {
                gameSound.PlaySFX(sfxs.Lose);
            }
            base.MG_End(_win);


        }

        public override void MG_Pause()
        {
            throw new System.NotImplementedException();
        }

        public override void MG_Quit()
        {
            throw new System.NotImplementedException();
        }

        public override void MG_Reset()
        {
            throw new System.NotImplementedException();
        }

        public override void MG_Start(int _id)
        {
            base.MG_Start(_id);
            actLevel = levels[_id];
            SetUI(actLevel);
        }

        public void Click(SolutionKey _sol)
        {
            Debug.Log("SolutionKey press");
            if (_sol.val == EMPTY_LETTER)
            {
                gameSound.PlaySFX(sfxs.SolutionPressFail);
            }
            else
            {
                gameSound.PlaySFX(sfxs.SolutionPressOk);

                _sol.filledMe.val = _sol.val;
                _sol.val = EMPTY_LETTER;
            }
        }

        public void Click(PossibleKey _pos)
        {
            //Debug.Log("PossibleKey press");
            if (_pos.val == EMPTY_LETTER)
            {
                gameSound.PlaySFX(sfxs.PossiblePressFail);
            }
            else
            {
                gameSound.PlaySFX(sfxs.PossiblePressOk);

                SolutionKey _sol = solutionKeys.Find(x => x.val == EMPTY_LETTER);

                _sol.filledMe = _pos;
                _sol.val = _pos.val;
                _pos.val = EMPTY_LETTER;

                _sol = solutionKeys.Find(x => x.val == EMPTY_LETTER);

                if (_sol == null) // si no me quedan espacios en blanco
                {
                    //Debug.Log("Checkeo win");
                    //Verificar win

                    int ii = 0;
                    bool _win = solutionKeys.TrueForAll(x =>
                    {
                        return x.val == actLevel.question.name[ii++].ToString().ToUpper();
                    });

                    MG_End(_win);
                }
            }
        }

        public void SetUI(TriviaImagesLevel value)
        {
            Debug.Log("Setting up UI TriviaImages");

            image.sprite = value.question;
            string _sol = value.question.name.ToUpper();

            solutionInstantiate.Clear(); solutionKeys.Clear();
            possibleInstantiate.Clear(); possibleKeys.Clear();

            foreach (char letter in _sol)
            {
                SolutionKey _keySol = solutionInstantiate.Get().GetComponent<SolutionKey>();
                _keySol.val = EMPTY_LETTER;
                solutionKeys.Add(_keySol);
                _keySol.button.onClick.RemoveAllListeners();
                _keySol.button.onClick.AddListener(()=> { Click(_keySol); });
            }

            List<char> _pos = new List<char>(_sol);

            int _simbolsLength = simbols.Length;

            for (int _ii = 0, _iiEnd = POSSIBLE_MAX - _sol.Length; _ii < _iiEnd; _ii++)
            {
                _pos.Add(simbols[Random.Range(0, _simbolsLength)]);
            }

            _pos.Shuffle();

            foreach (char letter in _pos)
            {
                PossibleKey _possible = possibleInstantiate.Get().GetComponent<PossibleKey>();
                _possible.val = letter.ToString().ToUpper();
                possibleKeys.Add(_possible);
                _possible.button.onClick.RemoveAllListeners();
                _possible.button.onClick.AddListener(() => { Click(_possible); });
            }

        }

        public override int GetNumLevels()
        {
            return levels.Length - numMixLevels;
        }



        void ShuffleableMinigame.Shuffle()
        {
            levels.Shuffle();
        }
    }
    [System.Serializable]
    public class SFX
    {
        public AudioClipReference SolutionPressFail;
        public AudioClipReference SolutionPressOk;
        public AudioClipReference PossiblePressFail;
        public AudioClipReference PossiblePressOk;
        public AudioClipReference Win;
        public AudioClipReference Lose;
    }
}