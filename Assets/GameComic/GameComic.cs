﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BoletaGaming.Comic
{
    public class GameComic : MonoBehaviour
    {
        [SerializeField] UnityEvent onEnable;
        [SerializeField] UnityEvent onNextPage;
        [SerializeField] UnityEvent onPrevPage;
        [SerializeField] UnityEvent onDisable;

        //public GameComicPage 
        public Image image;
        public Sprite[] pages;
        public int actPage = 0;

        [SerializeField] Button btnNextPage;
        [SerializeField] Button btnPrevPage;
        [SerializeField] Button btnSkip;

        private void OnEnable()
        {
            GoToPage(0);
            onEnable.Invoke();
        }
        private void OnDisable()
        {
            onDisable.Invoke();
        }

        public void GoToPage(int indexPage)
        {
            // we assume indexPage is valid jeje
            this.actPage = indexPage;

            image.sprite = pages[indexPage];
        }

        public void NextPage()
        {

            actPage++;
            if(actPage >= pages.Length)
            { // salgo del libro por la derecha
                actPage--;
                gameObject.SetActive(false);
                return;
            }
            onNextPage.Invoke();
            GoToPage(actPage);

        }

        public void PrevPage()
        {
            actPage--;
            if (actPage < 0)
            { // salgo del libro por la izquierda
                actPage++;

                return;
            }
            onPrevPage.Invoke();
            GoToPage(actPage);
        }
    }
}
