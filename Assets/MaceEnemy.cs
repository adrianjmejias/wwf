﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace BoletaGaming.NewPlatformer2D
{
    public class MaceEnemy : MonoBehaviour
    {
        private Rigidbody2D rb;
        private Vector3 dir;
        private Vector3 initialPos;
        public float speed;
        // Start is called before the first frame update
        void Start()
        {
            initialPos = transform.position;
            rb = GetComponent<Rigidbody2D>();
            dir = Vector3.down;
        }

        void Update()
        {
            if (transform.position.y > initialPos.y)
            {
                dir = Vector3.down;
            }

            rb.velocity = dir * speed;
        }

        void OnCollisionEnter2D(Collision2D other)
        {
            switch (other.gameObject.tag)
            {
                case "Indestructible":
                    dir = Vector3.up / 2;
                    break;

                case "Player":
                    FindObjectOfType<GameManager>().KillPlayer();
                    break;
                case "Enemy":
                    
                    break;


                default:

                    break;
            }
        }

    }

}
