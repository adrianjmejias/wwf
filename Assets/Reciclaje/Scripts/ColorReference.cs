﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New ColorReference" , menuName = "ColorReference")]
public class ColorReference : ScriptableObject {

    public Color color;
}
