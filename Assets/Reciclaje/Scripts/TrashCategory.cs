﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New TrashCategory", menuName = "TrashCategory")]
public class TrashCategory : ScriptableObject
{
    public Color color;
    public List<AudioClip> pickAudios;
    
}
