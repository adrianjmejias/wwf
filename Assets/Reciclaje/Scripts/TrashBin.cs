﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


namespace BoletaGaming.Reciclaje
{
    public class TrashBin : MonoBehaviour, IDropHandler, IPointerClickHandler, IPointerEnterHandler
    {
        [SerializeField] Text textCategory;
        public TrashCategory category;
        private void Start()
        {
            textCategory.text = category.name;
            GetComponent<Image>().color = category.color;
        }
        public void OnDrop(PointerEventData eventData)
        {
            Trash itemDragged = Trash.itemBeingDragged;
            Debug.Log("OnDrop " + name);
            
            if (itemDragged)
            {
                if (category.GetInstanceID() == Trash.itemBeingDragged.category.GetInstanceID())
                {

                    FindObjectOfType<Reciclaje>().totalTrash--;
                    //o ejecutar animacion
                    itemDragged.Botar(this);
                    Trash.itemBeingDragged = null;
                }
            }
        }

        void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
        {
            var itemDragged = Trash.itemBeingDragged;
            Debug.Log("OnPointerClick " + name);

            if (itemDragged)
            {
                if (category.GetInstanceID() == Trash.itemBeingDragged.category.GetInstanceID())
                {

                    //FindObjectOfType<Reciclaje>().totalTrash--;
                    //o ejecutar animacion
                    //Destroy(Trash.itemBeingDragged);
                }
            }
        }

        void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
        {
            var itemDragged = Trash.itemBeingDragged;
            Debug.Log("OnPointerEnter " + name);

            if (itemDragged)
            {
                if (category.GetInstanceID() == Trash.itemBeingDragged.category.GetInstanceID())
                {

                    //FindObjectOfType<Reciclaje>().totalTrash--;
                    //o ejecutar animacion
                    //Destroy(Trash.itemBeingDragged);
                }
            }

        }
    }
}

