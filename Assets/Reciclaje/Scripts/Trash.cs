﻿using System.Collections;
using System.Collections.Generic;
using BoletaGaming.AudioGraph;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


namespace BoletaGaming.Reciclaje
{
    public class Trash : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        [SerializeField] AudioNode audioNode;
        Image image;
        public TrashCategory category;
        public static Trash itemBeingDragged;
        Vector3 startPos;

        private void Awake()
        {
            image = GetComponent<Image>();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            image.raycastTarget = false;
            itemBeingDragged = this;
            //Debug.Log("OnBeginDrag");
            startPos = transform.position;

            audioNode.PlaySFX(category.pickAudios[Random.Range(0, category.pickAudios.Count)]);
            
        }

        public void OnDrag(PointerEventData eventData)
        {
            transform.position = Input.mousePosition;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            //Debug.Log("OnEndDrag");

            image.raycastTarget = true;
            itemBeingDragged = null;
            transform.position = startPos;
        }

        // Use this for initialization
        void Start()
        {
            GetComponent<Outline>().effectColor = category.color;
        }

        public void Botar(TrashBin papelera)
        {
            startPos = transform.position;
            transform.SetParent(papelera.transform);
            image.raycastTarget = false;
            StartCoroutine(BotarBasurish());
        }
        private IEnumerator BotarBasurish()
        {
            float timeGoal = 2;
            float time = 0;
            float rotByTime = 10;
            Vector3 initPos = transform.localScale;
            Vector3 zero = Vector3.zero;

            while (time < timeGoal)
            {
                transform.localScale = Vector3.Lerp(initPos, zero, time / timeGoal);
                transform.rotation *= Quaternion.Euler(0, 0, rotByTime);

                time += Time.deltaTime;
                yield return null;
            }
            Destroy(gameObject);
        }

    }
}
