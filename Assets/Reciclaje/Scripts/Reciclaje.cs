﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BoletaGaming.WWF.Minigame;
using BoletaGaming.WWF;

namespace BoletaGaming.Reciclaje
{

    public enum gameStates
    {
        playing,
        paused
    }

    public class Reciclaje : Minigame
    {
        public gameStates gameState = gameStates.paused;

        [SerializeField] Text counter;
        [SerializeField] Text textTimer;
        [SerializeField] Timer timer;
        [SerializeField] float timeForGame;
        public GameObject counterObj;

        public Transform canvasTransform;


        //private Map currentMap;
        private GameObject currentMapObj;
        public int totalTrash = -1;

        public AudioClip putTrashOnBin;
        public AudioClipReference win;
        public AudioClipReference lose;
        public BoletaGaming.AudioGraph.AudioNode audioNode;
        //[System.Serializable]
        //public class Map
        //{
        //    //public Image background;
        //    public List<Trash> trash;
        //}


        /* game variables */
        public List<GameObject> gameMaps;

        //public List<Map> gameMapsInfo;

        private void Update()
        {
            switch (gameState)
            {

                case gameStates.playing:

                    if (totalTrash == 0)
                    {
                        MG_End(true);
                        gameState = gameStates.paused;
                        return;
                    }

                    counter.text = "Quedan " + totalTrash.ToString() + " objetos restantes!";

                    break;

            }

        }

        public void StartGame(int indexMap)
        {
            if (currentMapObj)
            {
                Destroy(currentMapObj);
            }

            currentMapObj = Instantiate(gameMaps[indexMap], canvasTransform);

            totalTrash = currentMapObj.GetComponentsInChildren<Trash>().Length;

            int index = counterObj.transform.GetSiblingIndex();
            counterObj.transform.SetSiblingIndex(index + 1);

            gameState = gameStates.playing;
        }




        public override void MG_Reset()
        {
            throw new System.NotImplementedException();
        }

        public override void MG_Quit()
        {
            throw new System.NotImplementedException();
        }

        public override void MG_Pause()
        {
            throw new System.NotImplementedException();
        }

        public override void MG_Start(int _id)
        {
            base.MG_Start(_id);
            StartGame(_id);
            timer.StartTimer(timeForGame, 0.1f);
        }

        public override void MG_End(bool _win)
        {
            base.MG_End(_win);
            if (_win)
            {
                Debug.Log("you win!");
                audioNode.PlaySFX(win);
            }
            else
            {
                Debug.Log("you lose");
                audioNode.PlaySFX(lose);

            }
        }


        public int[] solvedLevels()
        {
            string solLevels = PlayerPrefs.GetString("PROGRESO_RECICLAJE");
            string[] strLevels = solLevels.Split(',');
            int[] numLevels = new int[strLevels.Length];

            for (int i = 0; i < strLevels.Length; i++)
            {
                int.TryParse(strLevels[i], out numLevels[i]);
                
            }

            return numLevels;
        }

        //public override void Save()
        //{
        //    //int numLevels = GetNumLevels();

        //    int[] sLevels = solvedLevels();

        //    foreach (int level in sLevels)
        //    {
        //        isFinished[level] = true;
        //    }

        //    List<string> levelsToSave = new List<string>();

        //    for (int i = 0; i < isFinished.Length; i++)
        //    {
        //        if (isFinished[i]) levelsToSave.Add(i.ToString());
        //    }

        //    var sb = new System.Text.StringBuilder();
        //    for (int i = 0; i < levelsToSave.Count; i++)
        //    {
        //        sb.AppendLine(i.ToString());
        //    }

        //    PlayerPrefs.SetString("PROGRESO_RECICLAJE", sb.ToString());
        //}

        public override int GetNumLevels()
        {
            return gameMaps.Count - numMixLevels;
        }


        public void UpdateTime()
        {
            System.TimeSpan t = System.TimeSpan.FromSeconds(timer.timeLeft);


            textTimer.text =

                string.Format("{0:D2}m:{1:D2}s",
                    t.Minutes,
                    t.Seconds
                );


            timer.timeLeft.ToString();
        }

    }

}
