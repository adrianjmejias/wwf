﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using BoletaGaming.WWF.Minigame;
using BoletaGaming.WWF;
using BoletaGaming.AudioGraph;
using BoletaGaming.HolderCombo;
using NaughtyAttributes;

namespace BoletaGaming.HiddenObjects
{
    public class HiddenObjects : Minigame
    {
        [BoxGroup("Database")] public Level[] levels;



        [BoxGroup("Sound")] public AudioClipReference AC_HIDDEN_CLICK;
        [BoxGroup("Sound")] public AudioClipReference WIN;
        [BoxGroup("Sound")] public AudioClipReference LOSE;
        [BoxGroup("Sound")] public AudioNode audioNode;

        [BoxGroup("UI")] [SerializeField] private Transform holderMain;
        [BoxGroup("UI")] [SerializeField] public HolderCombo.HolderCombo comboNameLink;
        [BoxGroup("UI")] [SerializeField] public HolderCombo.HolderCombo comboImageLink;
        [BoxGroup("UI")] [SerializeField] public HolderCombo.HolderCombo comboOK;

        [BoxGroup("State")] [SerializeField] private Level actLevel;
        [BoxGroup("State")] [SerializeField] private List<HiddenObject> hoImages = new List<HiddenObject>();
        [BoxGroup("State")] [SerializeField] private List<GameObject> hoLink = new List<GameObject>();


        public void ClickHiddenObject(HiddenObject clicked)
        {
            audioNode.PlaySFX(AC_HIDDEN_CLICK);
            PrintState();

            hoImages.Remove(clicked);
            hoLink.Remove(clicked.link);

            Destroy(clicked.gameObject);
            Destroy(clicked.link.gameObject);


            if (hoImages.Count > 0) // game keeps going
            {

            }
            else //game over
            {
                MG_End(true);
            }
        }

        void SetUI(Level value)
        {
            hoImages.ForEach(x => Destroy(x.gameObject)); hoImages.Clear();
            hoImages.ForEach(x => Destroy(x.gameObject)); hoLink.Clear();
            holderMain.Clear();
            comboImageLink.Clear();
            
            List<HiddenObject> _imgs = Instantiate(value.map,holderMain).GetComponentsInChildren<HiddenObject>().ToList();
            
            _imgs.ForEach(x =>
            {
                x.GetComponentInChildren<Button>().onClick.AddListener(()=> ClickHiddenObject(x));
                hoImages.Add(x);

                switch (x.type)
                {
                    case HiddenType.name:
                        x.SetLink(comboNameLink.Get());
                        Text tx = x.link.GetComponentInChildren<Text>();
                        tx.text = x.name;
                        x.link.name = x.name;
                        hoLink.Add(x.link);
                        break;
                    case HiddenType.description:
                        throw new System.NotImplementedException();
                    case HiddenType.silhouette:
                        x.SetLink(comboImageLink.Get());
                        Image im = x.link.GetComponentInChildren<Image>();
                        im.sprite = x.GetComponent<Image>().sprite;
                        im.color = Color.black;
                        x.link.name = x.name;
                        hoLink.Add(x.link);
                        break;
                    default:
                        break;
                }
            });
        }

        public void PrintState()
        {
            Debug.Log("there are " + hoImages.Count+" and "+ hoLink.Count);
        }

        public override void MG_Reset()
        {
            throw new System.NotImplementedException();
        }

        public override void MG_Quit()
        {
            throw new System.NotImplementedException();
        }

        public override void MG_Pause()
        {
            throw new System.NotImplementedException();
        }

        public override void MG_Start(int _id)
        {
            base.MG_Start(_id);
            actLevel = levels[_id];
            SetUI(actLevel);
            PrintState();
        }

        public override void MG_End(bool _win)
        {
            base.MG_End(_win);

            if (_win)
            {
                audioNode.PlaySFX(WIN);
            }
            else
            {
                audioNode.PlaySFX(LOSE);
            }
        }

        public override int GetNumLevels()
        {
            return levels.Length - numMixLevels;
        }
    }

    [System.Serializable]
    public class Level
    {
        public GameObject map;
    }

}

