﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoletaGaming.HiddenObjects
{
    public enum HiddenType
    {
        name, description, silhouette
    }

    public class HiddenObject : MonoBehaviour
    {
        [SerializeField] 
        public string description;
        public HiddenType type = HiddenType.name;
        public GameObject link;
        public void SetLink(GameObject link)
        {
            if (link)
            {
                Debug.Log("Link set" + name);
                this.link = link;
            }
        }
    }
}
