﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyChilds : MonoBehaviour
{
    private void OnDisable()
    {
        transform.Clear();
    }
}
