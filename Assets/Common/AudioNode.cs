﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using System.Linq;


namespace BoletaGaming.AudioGraph
{
    public class AudioNode : MonoBehaviour
    {
        public static AudioNode Instance;
        [SerializeField] private AudioNode masterRef;

        [BoxGroup("AudioSource")] [ReadOnly] private List<AudioSource> AS_SFX = new List<AudioSource>();
        [BoxGroup("AudioSource")] [ReadOnly] private AudioSource AS_BGM;
        


        private void OnEnable()
        {
            TrySetMaster(Instance);
        }


        public AudioNode GetMaster()
        {
            return masterRef;
        }

        public bool TrySetMaster(AudioNode _master)
        {
            if (!_master) {
                return false;
            }

            if (_master == this)
            {
                Debug.LogError("NO PUEDES SER TU PROPIO MASTER ESTÁS LOCOOOOO");
                return false;
            }

            HashSet<AudioNode> _chain = new HashSet<AudioNode>();
            AudioNode _aux = _master.GetMaster();
            while (_aux)
            {
                if (_chain.Contains(_aux))
                {
                    Debug.LogError("Can't set " + _master.name+ " as master because it would create a loop");
                    return false;
                }
                _chain.Add(_aux);
                _aux = _aux.GetMaster();
            }

            masterRef = _master;
            masterRef.SetSlave(this);
            return true;
        }


        public void SetSlave(AudioNode _AN)
        {
            this.AS_SFX.AddRange(_AN.AS_SFX);
        }

        public void PlayBGMS(List<AudioClipReference> _bgms)
        {
            PlayBGMS(_bgms);
        }

        public void PlaySFX(AudioClipReference _sfx)
        {
            PlaySFX(_sfx.value);
        }

        public void PlaySFX(AudioClip _sfx) 
        {
            if (masterRef) // caso que soy no principal
            {
                masterRef.PlaySFX(_sfx);
                return;
            }

            //Debug.Log("Dandole play a "+ _sfx.name+" en "+ gameObject.name);

            StartCoroutine(CR_PlaySFX(_sfx));
        }

        public void PlayBGMS(IList<AudioClip> _bgms)
        {
            if (masterRef) // caso que soy no principal
            {
                masterRef.PlayBGMS(_bgms);
                return;
            }

            StartCoroutine(CR_PlayBGM(_bgms));
        }

        public void StopBGMS()
        {
            if (masterRef)
            {
                masterRef.StopBGMS();
                return;
            }

            StopCoroutine("CR_PlayBGM");
        }

        //solo llamado por master
       IEnumerator CR_PlayBGM(IList<AudioClip> _bgms)
        {
            if (!AS_BGM)
            {
                AS_BGM = gameObject.AddComponent<AudioSource>();
            }

            while (true)
            {
                AudioClip _AC = _bgms.GetRandom();

                if (!_AC)
                {
                    Debug.LogError("No bgm track");
                    yield break;
                }

                AS_BGM.clip = _AC;
                AS_BGM.Play();
                yield return new WaitForSeconds(_AC.length);
            }
        }

        //solo llamado por master
        IEnumerator CR_PlaySFX(AudioClip _sfx)
        {
            if(AS_SFX.Count == 0)
            {
                AS_SFX.Add(gameObject.AddComponent<AudioSource>());

                //configs del audio source
            }
            AudioSource _AS = AS_SFX[0];
            AS_SFX.RemoveAt(0);

            if (!_sfx)
            {
                Debug.LogError(" NO SFX TRACK");
                yield break;
            }

            _AS.clip = _sfx;
            _AS.Play();


            yield return new WaitWhile(() => _AS.isPlaying);

            AS_SFX.Add(_AS);
        }



        public float SFXVol
        {
            set
            {
                if (masterRef)
                {
                    masterRef.SFXVol = value;
                }
                AS_SFX.ToList().ForEach(x => x.volume = value);
            }
        }

        public float BGMVol
        {
            set
            {
                if (masterRef)
                {
                    masterRef.BGMVol = value;
                }
                AS_BGM.volume = value;
            }
        }
    }
}


