﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;



namespace BoletaGaming.HolderCombo
{
    [System.Serializable]
    public struct HolderCombo
    {
        [SerializeField] private Transform holder;
        [SerializeField] private GameObject prefab;
        public GameObject Get()
        {
            return GameObject.Instantiate(prefab, holder);
        }
        public T Get<T>()
        {
            return GameObject.Instantiate(prefab, holder).GetComponent<T>();
        }
        public HolderCombo Clear()
        {
            holder.Clear();
            return this;
        }

        public void SetActive(bool active)
        {
            holder.gameObject.SetActive(active);
        }
    }
}
