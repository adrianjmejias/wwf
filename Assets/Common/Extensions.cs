﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Security.Cryptography;

public static class Extensions
{
    public static IList<T> Shuffle<T>(this IList<T> self)
    {
        RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();
        int n = self.Count;
        while (n > 1)
        {
            byte[] box = new byte[1];
            do provider.GetBytes(box);
            while (!(box[0] < n * (byte.MaxValue / n)));
            int k = (box[0] % n);
            n--;
            T value = self[k];
            self[k] = self[n];
            self[n] = value;
        }
        return self;
    }

    public static void Swap<T>(ref T a, ref T b){
        T aux = a;
        a = b;
        b = aux;
    }

    public static IList<T> ShuffleRange<T>(this IList<T> self, int from, int to)
    {
        int n = 2;
        from = Mathf.Max(from, 0);
        to = Mathf.Min(to, self.Count);
        while(n > 0)
        {
            for(int ii =from; ii<to; ii++)
            {
                int rand = Random.Range(from, to);
                T aux = self[ii];
                self[ii] = self[rand];
                self[rand] = aux;
            }
            n--;
        }

        return self;
    }

    public static bool IsIndexInArray<T>(this IList<T> self, int index)
    {
        return index < self.Count && index > -1;
    }

    public static T GetRandom<T>(this IList<T> self)
    {
        return self[Random.Range(0, self.Count)];
    }

    public static Transform Clear(this Transform self)
    {
        foreach (Transform child in self)
        {
            GameObject.Destroy(child.gameObject);
        }

        return self;
    }

    public static Vector3 Clamp(this Vector3 vec, float min, float max)
    {
        vec.x = Mathf.Clamp(vec.x, min, max);
        vec.y = Mathf.Clamp(vec.y, min, max);
        vec.z = Mathf.Clamp(vec.z, min, max);

        return vec;
    }

}
