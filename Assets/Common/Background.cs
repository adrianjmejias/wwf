﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Background : MonoBehaviour
{
    [SerializeField] Image background;
    [SerializeField] Sprite[] spriteBackground;

    private void Start()
    {
        background.gameObject.SetActive(true);
    }

    private void OnEnable()
    {
        background.sprite = spriteBackground.GetRandom();
    }
}
