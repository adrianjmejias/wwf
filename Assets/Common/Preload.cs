﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Preload : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] Text text;
    [SerializeField] string mainSceneName;
    [SerializeField] Camera cam;

    public Color init, end;
    void Start()
    {
        text.text = "0%";
        var a = SceneManager.LoadSceneAsync(mainSceneName);
        Debug.Log("hola");
        a.allowSceneActivation = true;
        StartCoroutine(CR_Start(a));
        a.completed += (jej) => { Debug.Log("jej is done");  };
    }

    private IEnumerator CR_Start(AsyncOperation op)
    {
        while (!op.isDone)
        {
            cam.backgroundColor = Color.Lerp(init, end, op.progress);
            text.text = String.Format("{0:.##}%", op.progress*100);
            yield return null;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Space))
        {
        }
    }


}
