﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PPWrapper
{
    public static void Set<T>(string key, T value)
    {
        if (!typeof(T).IsSerializable)
        {
            throw new System.Exception("BG -- Value is not serializable");
        }

        PlayerPrefs.SetString(key, JsonUtility.ToJson(value));
    }


    public static T Get<T>(string key)
    {
        if (!typeof(T).IsSerializable)
        {
            throw new System.Exception("BG -- Value is not serializable");
        }

        if (!PlayerPrefs.HasKey(key))
        {
            throw new System.Exception("BG -- Key Was not found");
        }


        return JsonUtility.FromJson<T>(PlayerPrefs.GetString(key));
    }
}
