﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothFollow : MonoBehaviour
{
    [SerializeField]
    public Transform target;

    [Range(0, 1)]
    public float t = 0.1f;

    private void Update()
    {
        if (target)
        {
            transform.position = Vector3.Lerp(transform.position, target.position, t);
        }
    }
}
