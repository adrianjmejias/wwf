﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BoletaGaming.WWF.Minigame;

public class MG_Tester : MonoBehaviour
{
    [SerializeField] Minigame testingMinigame;
    [SerializeField] int actLevel;
    [SerializeField] KeyCode nextLevel = KeyCode.RightArrow;
    [SerializeField] KeyCode prevLevel = KeyCode.LeftArrow;
    [SerializeField] KeyCode play = KeyCode.Return;
    [SerializeField] KeyCode endWin = KeyCode.UpArrow;
    [SerializeField] KeyCode endLose = KeyCode.DownArrow;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(nextLevel))
        {
            actLevel++;
        }

        if (Input.GetKeyDown(prevLevel))
        {
            actLevel--;

        }

        if (Input.GetKeyDown(play))
        {
            testingMinigame.MG_Start(actLevel);
        }

        if (Input.GetKeyDown(endWin))
        {
            testingMinigame.MG_End(true);
        }

        if (Input.GetKeyDown(endLose))
        {
            testingMinigame.MG_End(false);
        }

    }
}
