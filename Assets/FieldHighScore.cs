﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FieldHighScore : MonoBehaviour
{

    [SerializeField] Text playerName;
    [SerializeField] Text score;
    public void Set(string playerName, int score)
    {
        this.playerName.text = playerName;
        this.score.text = score.ToString();
    }
}
