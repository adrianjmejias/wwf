﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Timer : MonoBehaviour
{
    [SerializeField] private float TimeSet = 5;
    [SerializeField] private float Tick = 0.2f;
    public float timeLeft
    {
        get
        {
            return pimeLeftt;
        }
    }

    public float timePassed
    {
        get
        {
            return TimeSet - pimeLeftt;
        }
    }
    private float pimeLeftt;

    private float _tick = 5;
    bool active = false;

    /// <summary>
    /// This is called every frame
    /// </summary>
    [SerializeField] UnityEvent onTick;

    /// <summary>
    /// This is called every _tick seconds
    /// </summary>
    [SerializeField] UnityEvent onTimedTick;

    [SerializeField] UnityEvent onFinish;
    [SerializeField] UnityEvent onStart;
    //[SerializeField] UnityAction onCancel;

    // Update is called once per frame
    void Update()
    {
        if(active)
        {
            pimeLeftt-=Time.deltaTime;
            _tick -= Time.deltaTime;

            if(pimeLeftt > 0)
            {
                if (_tick < 0)
                {
                    _tick = Tick;
                    onTimedTick.Invoke();
                }
                onTick.Invoke();
            }
            else
            {
                pimeLeftt = 0;
                active = false;
                onFinish.Invoke();
            }
        }
    }


    public void StartTimer(float timemax, float timetick)
    {
        TimeSet = timemax;
        Tick = timetick;

        pimeLeftt = TimeSet;
        _tick = Tick;
        active = true;
        onStart.Invoke();
    }

    private void OnDisable()
    {
        active = false;
    }
}
