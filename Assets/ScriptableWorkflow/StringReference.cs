﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Create string Reference")]
public class StringReference : ScriptableReference<string>
{

}
