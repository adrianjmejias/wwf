﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Create LIST REFERENCE AudioClip Reference")]
public class ListReferenceAudioClipReference : ListReference<AudioClipReference>
{

}
