﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Create list AudioClip Reference")]
public class ListReferenceAudioClip : ListReference<AudioClip>
{
}

