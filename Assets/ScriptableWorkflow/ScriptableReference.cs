﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ScriptableReference<T> : ScriptableObject
{
    public T value;
    // public static
}
