﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Create List Image Reference")]
public class ListReferenceImage : ListReference<UnityEngine.UI.Image>
{

}
