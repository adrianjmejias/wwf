﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListReference<T> : ScriptableReference<List<T>>
{
    public T Remove(T obj)
    {
        T re = value.Find(x => x.Equals(obj));
        if (re != null)
        {
            value.Remove(re);
        }
        return re;
    }
    public ListReference<T> Add(T obj)
    {
        value.Add(obj);
        return this;
    }
    public T Find(System.Predicate<T> pre)
    {
        return value.Find(pre);
    }
    public int Count
    {
        get
        {
            return value.Count;
        }
    }
    public void ForEach(System.Action<T> act)
    {
        value.ForEach(act);
    }
    public void Clear()
    {
        value.Clear();
    }
}
