﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Create AudioClip Reference")]
public class AudioClipReference : ScriptableReference<AudioClip>
{

}
