﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Create Sprite Reference")]
public class SpriteReference : ScriptableReference<Sprite>
{

}
