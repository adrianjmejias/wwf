﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Create int Reference")]
public class IntReference : ScriptableReference<int>
{

}
