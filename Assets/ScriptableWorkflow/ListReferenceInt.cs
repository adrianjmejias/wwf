﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Create list int Reference")]

public class ListReferenceInt : ListReference<int>
{
}
